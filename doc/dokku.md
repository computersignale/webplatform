# dokku

[dokku](https://github.com/dokku/dokku) is a minimalistic PaaS. We use it to run the docker image and use the letsencrypt plugin for SSL. In the following documentation DOKKU_HOST is the server dokku is running on, ie. "DOKKU_HOST" should be replaced with the ip address of the server.

## dokku apps

to list the apps, ssh to the dokku host and run:

    dokku apps:list

## dokku config

    dokku config --app csweb-prod

    =====> csweb-prod env vars
    DOKKU_APP_RESTORE:        1
    DOKKU_APP_TYPE:           dockerfile
    DOKKU_DOCKERFILE_PORTS:   80
    DOKKU_LETSENCRYPT_EMAIL:  no-reply@zhdk.ch
    DOKKU_PROXY_PORT_MAP:     http:80:80 https:443:80
    DOKKU_PROXY_SSL_PORT:     443
    GIT_REV:                  09e07b65f6fb58f8cb83aa803feb011dadf8aaaa
    NO_VHOST:                 0

## Domains & proxy

    dokku domains:report
    dokku proxy:report

## SSL

check status:

    dokku letsencrypt:ls

letsencrypt commands help

    dokku letsencrypt:help


## dokku commands via ssh

dokku commands can be executed directly on the remote via ssh using the `dokku` user.
example to check log files, from your computer run (replace DOKKU_HOST with ip of the server):

    ssh -t dokku@DOKKU_HOST logs csweb-prod -t


## deploy app manually

### development:

add remote (if not done yet):

    git remote add dokku-dev dokku@DOKKU_HOST:csweb-dev

deploy:

    git push dokku-dev develop

### production


add remote (if not done yet):

    git remote add dokku-prod dokku@DOKKU_HOST:csweb-prod

deploy:

    git push dokku-prod master
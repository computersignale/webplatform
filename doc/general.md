# Intro
The Computersignale website is a custom made platform focusing on a better reading experience for long and media-rich articles. By displaying text in a paged layout with columns we create a book like reading experience. These pages are currently only found inside the "Rigi" section of the website. In addition the application generates three different views into the articles that aim to help maintain an overview:
1. Abstract: The abstract and a generated table of contents
2. Text: The main text in a paged layout
3. Outline: An abstraction of the text, focusing on structure and media

See [page types](#page-types) to learn more about other types of pages.

The main challenge was to design and develop the concept for the new reading experience. The application makes heavy use of new web technlogies like the Fetch API, History API, Promises, Mutation Observers, Arrow functions, CSS Custom Properties, CSS Animations and more.
Another big challenge was to support all device sizes while maintaining the usibility and functionality of the reading experience and the different views.

# Technology
The core website is custom built and does not rely on any third-party framework. All website calls are redirected to index.html and handled by javascript wich acts as a single controller.

Used third-party scripts:
* [elasticlunr.js](http://elasticlunr.com/) (search engine)
* [showdown.js](http://showdownjs.com/) (markdown converter)

## pages.json file
The pages.json file lists all pages of the website. Every page has a route, a content and a titles object. To add a new page just add a new object with the desired values. The order of the objects currently has no effect.

## Page types
There are three types of pages. All pages load the index.html file as a default. The type of the page is determined by the content value of the according object in the pages.json file. See [use cases](#use-cases) to learn more about creating new pages.

1. **Static HTML pages (`*.html` defines a static html page)**
    As the name suggests, static pages load a static HTML file into the #page tag. Most pages currently are static HTML pages.
2. **Article pages (`*.md` defines a article page)**
    These pages are all based on a markdown text file. The default index.html file is loaded and then a custom view into the markdown file is generated. These pages are automated and complex views of the markdown file. They are currently only used for the Rigi Discussion.
3. **Script pages (`*()` defines a script page)**
    Script based pages run a custom javascript function after loading the index.html file. Currently the only script page is the cichlid texas page.
    These pages allow to embed custom applications into the page.

## Markdown syntax
The markdown files used to generate the Rigi Discussion are valid standard markdown. They are located in `/components/articles`. The following custom rules apply:

* The Meta- or Headerblock uses the custom delimiter
    ```
    ---
    ---
    ```
* Media collections can be defined by chaining empty link elements as shown in the following example (the description can not contain brackets!):
`[MyCollection](myfirstfile.wav "myfirstfile")[](mysecondfile.png "mysecondfile")`
* To introduce a new speaker use the following syntax `(SpeakerName:)`. The brackets will be removed later by the script. This functionality was introduced to allow for speechsynthesis in a later stage but has currently no further use.

## Multilanguage support
Currently the page supports two languages, English and German. There are two "systems" in place to allow for multilanguage support. Individual content files and a strings.json file. The language is determined via the first part of the URL `en/` or `de/` and saved in a sessionsStorage item called "lang". It can be changed by using the language button in the header section of the page or by manually changing the URL.

### Individual content files
Every page has an individual content file for each language. This also goes for the articles and overlay pages like the Menu or Search. This creates a lot of files but is the easiest way to handle it for now. Handling all strings via a json file is not feasible due to too a lot of content that uses more complex DOM structures (for example the impressum).

### strings.json file
To translate strings in the index.html file and handle strings inserted via main.js the strings.json file has been created. Currently it only handles a hand full of strings but could be used within other content files by inserting the following `<span data-string="name-of-string"></span>` as well. After the page has been loaded the script replaces the contents of the tag with the string for the current language. To use these strings within the main.js script use `stringList["name-of-string"]`.
If the object contains a field named "markdown" the contents will be parsed as markdown:

```
"show-in-article": {
    "en": "Show in article",
    "de": "Im Artikel anzeigen"
}
```

or

```
"show-in-article": {
    "markdown" : "true",
    "en": "#Show in \n\n article",
    "de": "#Im Artikel \n\n anzeigen"
}
```

## Exceptions and Remarks

### App body flexbox or not
By default the app__body element expands to the height of the content. For a page to automatically expand to the size of the browser viewport (flexbox) add it to the global array `flexPages`. Currently all article pages in "read view" are automatically rendered as flexbox.

### Intro article by Hansjörg Rheinberger
The intro by Hans-Jörg Rheinberger is an exception to the Article pages and only has a read view. Meta and Outline views are hidden via js. 

### Images for PDF files
Since we cannot create a preview into a PDF file, each PDF file requires a png thumbnail with the same name in the same directory and a recommended width of 500px. An error will be thrown if no thumbnail is provided. Example: myFile.pdf -> myFile.png

### Images for video files
Videofiles require a png thumbnail with the same name in the same directory and recommended with of 500px. An error will be thrown if no thumbnail is provided.
    Example: myFile.mp4 -> myFile.png

### Images for audio files
Audiofiles require a png thumbnail with the same name in the same directory and recommended with of 500px. An error will be thrown if no thumbnail is provided.
    Example: myFile.wav -> myFile.png

### Images for media collections
Media collections can contain various file types (audio, video, images). In case the first item is not a png image we have to provide an extra thumbnail with the same name as the first item in the collection. If none is provided an error will be thrown. Recommended width for thumbnails is 500px. The same image will also be used when viewing audio items.
    Example: `[MyCollection](myfirstfile.wav "myfirstfile")[](mysecondfile.png "mysecondfile")` -> myfirstfile.png

## Search engine
As search engine the [elasticlunr](http://www.elasticlunr.com/) is beeing used. A static searchindex file has to be [created manually](#update-searchindex) each time the contents of the website are updated.
The configuration and fields for the searchindex are defined in *scripts/makeSearchIndex.js* while the generated index can be found in */searchIndex.json*.
Since there is not so much content, another option could be to generate the searchindex on the clientside everytime the user loads the page. This way the index does not have to be updated manually.

## Stylesheet (main.less/main.css)
* [LESS](http://lesscss.org/) is used as CSS preprocessor
* The less file is used in development only. The website works directly with the css file. Convert to css with your favorite text editor or manually via a web converter.
* icon-font.css defines the styles for the icons and is included via import in the main.css file
* CSS Naming conventions according to [BEM](http://getbem.com/) are beeing used to keep the names more consistent.
* The default styles are defined for small screens. Specific styles for wider Screens are defined within media queries.
* Fonts are embedded directly via google fonts link in the index.html file.

## Javascript (main.js)

### User Interactions
All events are registered to the "app" element. This way events targeting dynamically loaded content (via Fetch API) will work even if the content has not been loaded yet. This has the advantage that all eventlisteners can be placed in one place instead of beeing distributed all over the file.

### State object
The state object is used by the History API to pass custom blocks of information with a new browser history entry. In the application we use the same object to build new pages, this makes it easier to pass information between the History API and the application.

* State Object example without article
    ```
    {"route": "/home"}
    ```
    
* State Object example with article parameters
    ```
    {
        "route": "/rigi/data",
        "article": {
            "char": "",
            "hash": "",
            "pct": "5",
            "view": "outline"
        }
    }
    ```

* State Object example with texas panorama parameters
    ```
    {
        "route": "/rigi/data",
        "panorama": {
            "loc": "fishfacilities",
            "time": "12:00",
            "display": "s",
            "states": "1,0",
            "play": "play"
        }
    }
    ```

## Deployment process
A public [GitLab Repository](https://gitlab.zhdk.ch/computersignale/webplatform) hosted at ZHDKs own GitLab server is beeing used. The public has read but not write permissions. The repository has a master and development branch.
The development branch is currently exposed to [http://computersignale.zhdk.ch:8000/](http://zhdk.computersignale.ch:8000/) while the master branch is on [http://computersignale.zhdk.ch/](http://zhdk.computersignale.ch/).
Currently the content on the webserver has to be updated manually by accessing the servers via ssh and updating the repository. To further simplyfy the deployment process, webhooks could be installed.
* `webprod/` directory for production (master branch, computersignale.zhdk.ch)
* `webdev/` directory for development (develop branch, computersignale.zhdk.ch:8000)

## Development workflow
This describes the recommended workflow for general development:
1. @local: develop in your local environment
2. @local: test code in your browser via a docker webserver
3. @local: commit and push changes to development branch in git repository
4. @remote: pull development branch to `/webdev`workfloqw directory on the remote webserver
5. @remote: test changes in your browser via http://computersignale.zhdk.ch:8000
6. @local: if satisfied merge development branch into master branch
7. @remote: pull master branch to `/webprod` directory on the remote webserver
8. @remote: test and review changes in your browser via http://computersignale.zhdk.ch

# Use cases

## Update searchindex
After new content has been added to the website the searchindex should be updated to make the new contents searchable. This procedure could be automated on the serverside in the future.
1. uncomment the "make search index" part at the bottom of index.html
2. open the browser and navigate to index.html
3. copy the new search index from the browsers console into the clipboard
4. replace the content of searchIndex.json with the clipboard
5. re-comment the "make search index" part at the bottom of index.html

## Add an article page
1. copy markdown file into `/components/articles/myArticle.md`
2. add new object to `/pages.json`
    ```
    {
        "title": {
            "de": "Mein Artikel",
            "en": "My article"
        },
        "route": "/rigi/myArticle",
        "content": "myArticle.md"
    }
    ```
3. add links to the article on desired pages with the `link--page` class
    ```
    <a class="link--page" href="/rigi/myArticle" title="My Article">My article</a>
    ```
    
## Add an html page
1. create an html file and place it in `/components/contents/content-myFile.html`
2. add new object to `/pages.json`
    ```
    {
        "title": {
            "de": "Meine Seite",
            "en": "My site"
        },
        "route": "/mysite",
        "content": "content-myFile.html"
    }
    ```
3. add links to the article on desired pages with the `link--page` class
    ```
    <a class="link--page" href="/mysite" title="My site">My site</a>
    ```
4. In the browser navigate to the new page via `/mysite`

## Add a new script page
1. create a script file with a function and place it in `/scripts/myScript.js`
    ```
    function myFunction() {
        let node = document.createElement("div");
        node.innerHTML = "some text";
        document.getElementById("page").appendChild(node);
    }
    ```
2. add new object to `/pages.json`
    ```
    {
        "title": {
            "de": "Meine Skriptseite",
            "en": "My scriptpage"
        },
        "route": "/myscriptpage",
        "content": "myScriptFunction()"
    }
    ```
3. add script to the bottom of the `index.html` file
    ```
    <script src="/scripts/myScript.js"></script>
    ```
4. In the browser navigate to the new page via `/myscriptpage`

## Update article text
If you update the contents of an articles markdown file nothing has to be done in addition. The application automatically generates the contents with the updated file.

## Adding link pointing to local pages
To create links that point to a local page just add the `link--page` class to the `<a>`-tag.

`<a class="link--page" href="/mysite" title="My site">My site</a>`

## Add a media link to a page
Media links in articles come with a mouse over preview and will open in a custom overlay. If you want to add this media link functionality to a media link just add the `link--media` class to the `<a>`-tag.

`<a href="/media/myFile.pdf" class="link--media">MyFile Link</a>`

# Known issues and remarks
In order to allow external, non-technical people to participate in improving the website we created a [google sheet](https://docs.google.com/spreadsheets/d/1iLVVuvUjE7OvNDXBO0xpES3YZ2V6u3Nv1ruYV5JIUt8/edit?usp=sharing) file to track issues states. It's beeing used for technical, design and content issues at the same time.

In Safari, hovering list items lead to a sudden jump to the top of the page. The issue was solved by setting the list-style-type to "none" and adding a custom string to the list item instead. The reason for this behaviour is completely unknown.

# Other
For older prototypes, usibility tests and smaller experiments a [second repository](https://gitlab.zhdk.ch/computersignale/weblabor) called weblabor has been created on GitLab.
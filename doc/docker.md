# How to use the webplaform with Docker

Build the image (once and from the repo root directory):

`docker build -t cswebplatform .`

For the two possible development setups create two different containers.

## Simple setup

Run the image on port 8000 with a syncronized code directory for the first time:

`docker run -d --name cswebplatform1 -p 8000:80 -v $(pwd)/app/:/html/:ro --restart unless-stopped cswebplatform`

Check the website at <http://localhost:8000>, change the local code and watch your browser again.

If you want, stop the container:

`docker stop cswebplatform1`

And later start it again:

`docker start cswebplatform1`

## Advanced setup with mounted fileshare `computersignale2`

Run the image on port 8001 with a syncronized code and media directories for the first time:

`docker run -d --name cswebplatform2 -p 8001:80 -v $(pwd)/app/:/html/:ro -v /Volumes/computersignale2/AUSTIN/www-media-tx/:/media-tx/media/tx/:ro --restart unless-stopped cswebplatform`

Watch a video that lies on the fileshare in your browser: <http://localhost:8001/media/tx/1/00-00.mp4>.

Stop and start the container:

`docker stop cswebplatform2`

`docker start cswebplatform2`

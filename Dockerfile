FROM nginx:alpine

COPY data/nginx/nginx.conf /etc/nginx/nginx.conf

#VOLUME /html
RUN mkdir /html/
ADD . /html/

VOLUME /media-tx/media/tx

EXPOSE 80
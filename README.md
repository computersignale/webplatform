# Webplatform
Computersignale II
Webplatform for an enhanced, scientific reading experience.

[Documentation](doc/index.md)

## Department:
IFCAR/ZHdK
Zurich University of the Arts

## Team:
- Hannes Rickli
- Birk Weiberg
- Valentina Vuksic
- Jan Huggenberg
- Christoph Stähli
# *in silico*

In einigen Bereichen der Biologie spielt sich Forschung heute fast ausschliesslich im Datenraum von Grossrechnern ab. Herangezogen und ausgewertet werden Datensätze, die häufig gar nicht mehr selbst erzeugt, sondern aus Repositorien bezogen worden sind. Ein solches Projekt hat im Mittelpunkt des letzten Teils unseres Gesprächs gestanden. Ausgangspunkt hierfür bildet ein Phänomen, das Biologen als Developmental Hourglass bezeichnen: In einer bestimmten Phase ihrer Entwicklung ähneln sich die Embryonen verschiedener Arten, während sie sich vorher und hinterher in ihrem Aussehen deutlich unterscheiden. Mit einem Poster vor Augen sprechen wir über Vorgehensweisen, Analyseschritte, auftretende Probleme und den Status der gewonnenen Einsichten. Was heisst es, *in silico* zu forschen?
---
---

## Developmental Hourglass

(Hans Hofmann:) Ich zeige euch hier ein aktuelles Projekt meines Labors, in dem **grosse Datensätze**, die frei verfügbar sind, mit neuen Methoden analysiert werden und dadurch neue Einsichten in althergebrachte biologische Fragen erlauben. Auf dem [Poster](In_Silico_01_Testing_the_hourglass.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*"), das ihr hier seht, geht es konkret um ein altes biologisches Problem, das bis heute ungelöst ist. Ich habe dieses Beispiel gewählt, weil es einen historischen Kontext hat, der für euch vielleicht interessant ist. [Karl Ernst von Baer](https://de.wikipedia.org/wiki/Karl_Ernst_von_Baer "Karl Ernst von Baer") hat Mitte des 19. Jahrhunderts mikroskopisch Embryonen vieler verschiedener Arten untersucht und festgestellt, dass sie am Anfang der Embryonalentwicklung sehr unterschiedlich aussehen und dann später auch. Dazwischen gibt es aber eine Periode, in der sie sich eher ähnlich sind.[^1]

(Hans-Jörg Rheinberger:) Auf Säugetiere bezogen?

(Hans Hofmann:) Ursprünglich auf Vertebraten, Wirbeltiere, bezogen, aber dieselbe Beobachtung wurde später auch bei Würmern und Arthropoden, Gliedertieren, gemacht. Das ist also nicht auf Wirbeltiere beschränkt. Dieses Phänomen wurde irgendwann einmal als Developmental Hourglass bezeichnet. Die Frage war, was zu dieser Reduktion der Variabilität führt und welche Mechanismen wirksam sind. In den 1920er Jahren wurde das noch einmal bestätigt, und in den letzten fünf, sechs Jahren erschienen ein paar Aufsätze, in denen genomweit Expressionsprofile angeschaut worden sind, also [Transcriptomics](https://de.wikipedia.org/wiki/Transkriptom "Transkriptom") betrieben wird.[^2] Dabei zeigt sich, dass die Ähnlichkeit – hier als Korrelationskoeffizienten gezeigt – der Genexpressionsprofile in dieser Periode signifikant grösser ist als in den früheren oder späteren Embryonalstadien. Diese Periode wird nach Klaus Sander als phylotypisches Stadium (Phylotypic Stage/Period) bezeichnet.[^3] Sie ist typisch für ein Phylum, zum Beispiel die Vertebraten. Nach diesen Papers zu urteilen, scheint das die molekulare Basis für das Ganze zu sein. Interessant ist: Was passiert da, und warum ist das so? Rudy Raff, der aus dem **Evo-Devo**-Kontext kommt – das sind die Leute, die sich dafür interessieren, wie sich die Steuerung der Individualentwicklung evolutionär herausgebildet hat –, hat dazu in den 1990er Jahren eine Hypothese vorgestellt.[^4] Genexpressionsnetzwerke, also die funktionellen Netzwerke, wären demnach früh und spät in der Embryonalentwicklung relativ isoliert voneinander. Im phylotypischen Stadium hingegen, in dem die Embryonen sich ähnlich sehen, wären sie sehr integriert. Die Grundidee dahinter ist, dass bei Pleiotropie – das heisst, wenn ein Gen auf viele verschiedene Mechanismen einwirkt oder viele verschiedene Prozesse steuert – ein Gen in der Evolution nicht so einfach verändert werden kann. Das ist ein Constraint, der zur Konsequenz hat, dass die Embryonen – sowohl was die Genexpression angeht als auch was die Morphologie angeht – relativ ähnlich aussehen.

(Hans-Jörg Rheinberger:) Auf welcher Grundlage hat Rudy Raff diese Hypothese damals entwickelt? Er hatte ja noch nicht die Techniken und die Daten, die wir heute zur Verfügung haben.

(Hans Hofmann:) Anfang der 1990er Jahre hat er einfach die alte Literatur rausgekramt und gesagt: Wir haben ein Problem, wir müssen das erklären – und hier ist eine Hypothese. Er hatte keinerlei Evidenz oder Datengrundlage dafür. Die Angelegenheit ist dann wieder in Vergessenheit geraten, bis vor fünf, sechs Jahren endlich die **Technologie** so weit war. Bislang hat sich aber keiner diese Netzwerke angeschaut und die Hypothese praktisch getestet. Und genau das wollen wir machen.

(Christoph Hoffmann:) Wie muss die Technologie aussehen, damit man diese Hypothese testen kann?

(Hans Hofmann:) Notwendig ist, dass praktisch jedes einzelne Gen, seine Aktivität über die Entwicklungszeit, gemessen werden kann. Also Transcriptomics, und das für verschiedene Arten. Mit unseren [Partnern an der Michigan State University](http://hintzelab.msu.edu/ "Hintze Lab") arbeiten wir auch an *in silico* Evolution. Sie haben [*Markov network brains*](http://devosoft.org/a-quick-introduction-to-markov-network-brains/ "A quick introduction to Markov Network Brains") entwickelt, das sind praktisch Agenten. Jeweils eine Zelle repräsentiert ein Brain. Man kann sich das wie ein Gewebe vorstellen, das sich entwickelt und bestimmte Muster bildet; das wären die Phänotypen. Man kann dann die Evolution in kurzer Zeit *in silico*, im Rechner, laufen lassen, um zu sehen, ob über die Entwicklung dieser Artificial Life Organismen im Rechner auch ein Hourglass zu sehen ist oder nicht: ob es also eine Eigenschaft von Entwicklungsvorgängen generell ist. Das ist ein komplementärer Ansatz, ziemlich neu in Entwicklungsbiologie und Evo-Devo. Diese Forschungsgebiete sind generell theoriearm und haben relativ wenige quantitative Ansätze, ausser den klassischen Reaktion-Diffusion-Modellen (zum Beispiel das [Gierer-Meinhardt-Modell](http://www.scholarpedia.org/article/Gierer-Meinhardt_model "Gierer-Meinhardt model")). Bei den Transkriptomen hast du das Problem, dass du die orthologen Gene – das heisst, die korrespondierenden Gene – von all diesen verschiedenen Arten identifizieren musst. Wir machen das mit vergleichenden Analysen, es gibt aber auch andere Möglichkeiten. [In der Analyse, die ich euch jetzt zeige](In_Silico_01_Testing_the_hourglass_00.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico* Poster Mittlere Spalte"), haben wir die Daten für 2.500 Gene – eigentlich sind es ***orthologous gene groups***, aber wir nennen sie jetzt einfach mal Gene – für verschiedene Arten, im Moment nur für Vertebraten, über die Entwicklungszeit angeschaut. Sie machen etwas mehr als zehn Prozent des Genoms aus, was nicht schlecht ist. Wir haben den grössten **Datensatz**, der bisher dahingehend analysiert worden ist; alles Daten, die aus öffentlichen Datenbanken heruntergeladen worden sind. Es handelt sich sowohl um [Microarray](https://de.wikipedia.org/wiki/Microarray "Microarray") als auch um [RNAseq](https://de.wikipedia.org/wiki/RNA-Seq "RNA-Seq") Data, die wir dann auf verschiedene Art und Weise integriert haben.

(Christoph Hoffmann:) Das Integrationsproblem sparst du jetzt aus, weil es zu lange dauern würde?

(Hans Hofmann:) Das Integrationsproblem spare ich aus, weil es auch ziemlich ins Detail geht. Man sieht nun, dass es eine Veränderung der Korrelation gibt, als Mass der Ähnlichkeit der Genexpressionsmuster von frühen Embryonalstadien zu späten Embryonalstadien. Im Pharyngulastadium, das generell als phylotypisches Stadium angesehen wird, sind aber alle einander relativ ähnlich. [Die r-Werte](In_Silico_01_Testing_the_hourglass_05.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico* Poster Mittlere Spalte") indizieren, dass sie statistisch nicht voneinander unterschiedlich sind, während hier wieder die Ähnlichkeit um einiges abnimmt. Mit dem grösseren Datensatz können wir so sehen, dass sich das Hourglass auf Expressionsebene nicht ohne weiteres rekonstruieren lässt. Man kann die Daten auch anders in Early, Middle und Late Stadien kombinieren und zusammenfassen. Da sieht man ebenfalls, dass Early und Middle ungefähr ähnliche Korrelationskoeffizienten haben. Allerdings ist in Early viel mehr Variation als in Middle. Das könnte ein Hinweis sein, aber im Vergleich zu dem, was andere mit kleineren Datensätzen publiziert haben, scheint es kein sehr robustes Signal zu sein. Wir haben uns dann entschlossen, erst einmal die **Komplexität des Datensatzes** – das sind ja Hunderte von Millionen von Datenpunkten – zu reduzieren. Wir machen das [hier](In_Silico_01_Testing_the_hourglass_01.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: PCs clearly distinguish species and embryological stages Poster Mittlere Spalte Mitte") mit einer **Principal Component Analysis** (PCA; Hauptkomponentenanalyse), mit der die Komponenten identifiziert werden, die die verschiedenen Anteile der Gesamtstreuung (totale Varianz) in den Daten erklären. Die Hauptkomponente, die den grössten Anteil der Gesamtvarianz im Datensatz enthält, ist PC1 (Principal Component 1). Auf sie gehen 83 Prozent der Variation in den Daten zurück. Das wird wiederum für die Arten unterschieden: Fisch und Maus, Huhn und hier an der Seite die Froscharten *Xenopus laevis* und *Xenopus tropicalis*. Die Principal Components 1 und 2 ziehen gewissermassen die Arten auseinander, erklären also die Unterschiede zwischen den Arten. Wenn du dir dann Principal Component 3 anschaust, siehst du, wie die einzelnen Entwicklungsstadien von 1 bis 15 aufgedröselt werden. PC3 repräsentiert nur 3,1 Prozent der Variation, erklärt aber die Varianz über die Zeit. Diese Komponente repräsentiert also die Daten gemäss den verschiedenen Entwicklungsstadien. Principal Components 1 und 2 erklären hingegen die Artunterschiede, vor allem zwischen den Säugetieren, dem Fisch und den Fröschen auf der einen Seite und dem Huhn auf der anderen Seite. Am interessantesten für unsere Zwecke ist aber Principal Component 3 mit nur drei Prozent der Variation, aber eben diese drei Prozent erklären die Entwicklungsstadien von 1 bis 15.

(Hans-Jörg Rheinberger:) Und was ist unter einer Component zu verstehen?

(Hans Hofmann:) Das ist eine Komponente der Varianz in einem Datensatz. Das Ziel der Principal Component Analysis ist es, die Vielzahl statistischer Variablen in einem komplexen Datensatz durch eine geringere Zahl möglichst aussagekräftiger Linearkombinationen – die sogenannten Principal Components – anzunähern. Jede Variable (zum Beispiel das Expressionsniveau der untersuchten Gene) «lädt» auf eine dieser Hauptkomponenten.

(Hans-Jörg Rheinberger:) Sind Components Gencluster?

(Hans Hofmann:) Ja, in gewisser Hinsicht schon: jedes Gen lädt auf eine bestimmte Principal Component, trägt also zu einer bestimmten Principal Component bei. Es gibt ein paar Hundert Gene, die überwiegend auf PC3 laden, das heisst, zu dieser zeitlichen Struktur im Genexpressionsprofil beitragen. Und wir denken, dass diese Gene vielleicht das Hourglassverhalten zeigen.

(Hans-Jörg Rheinberger:) Könnt ihr sie identifizieren?

(Hans Hofmann:) Ja, es sind ein paar Hundert. [Ich zeige euch einfach mal](In_Silico_01_Testing_the_hourglass_01.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: PCs clearly distinguish species and embryological stages Poster Mittlere Spalte Mitte"), wie man so einen gigantischen Datensatz praktisch aufdröseln und aus verschiedenen Perspektiven betrachten kann. Darin steckt unglaublich viel Information. 3,1 Prozent der Variation, hört sich nicht viel an, aber das sind unter Umständen die Gene, an denen wir interessiert sind. Die wichtige Achse ist die Y-Achse.

(Christoph Hoffmann:) Und das ist eine Zeitachse?

(Hans Hofmann:) Nein, obwohl es so aussieht; das ist die Principal Component, also eine Komponente der Variation (von mehreren) im Datensatz. Für uns unerwartet spiegelt die Y-Achse nun die Zeitachse wieder: von 1, Early Embryo Development, bis 15, späte Embryonalphase – was darauf hinweist, dass die Gene, die auf diese Komponente laden, diejenigen sind, die die zeitliche Struktur und Veränderung während der Embryonalentwicklung erklären.

(Christoph Hoffmann:) Das heisst, die zeitliche Abfolge geht aus der Analyse hervor; sie ist nicht das, was vorausgesetzt ist?

(Hans Hofmann:) Genau. Deswegen war das so überraschend. Wir hatten nicht damit gerechnet, das zu sehen. Diese multivariaten Analysen sind gut geeignet, um in den Daten **Muster** zu erkennen, die man sonst überhaupt nicht sehen würde oder die man nicht erwarten würde. Hier haben wir dann versucht, die verschiedenen Embryonalstadien für die verschiedenen Arten zueinander in Beziehung zu setzen. Für das Huhn zum Beispiel ist von 1 bis 15 alles zusammengedrückt, wohingegen es für die anderen Arten gedehnter ist. Die Frage lautet: Wie setzt man das zueinander in Beziehung, was sind eigentlich die äquivalenten Embryonalstadien? Das ist ein relativ schwieriges Problem. Wir haben erst einmal – das ist alles Work in Progress, das ist ein Poster von einer Konferenz – in der klassischen Literatur nachgeschaut, wann die verschiedenen Entwicklungsstadien, die hier beschrieben sind, während der Entwicklung im Verhältnis zu diesem Somitenpaar auftauchen. Denn jeder Embryo ist in Segmente gegliedert, die Somiten genannt werden. Das trifft nicht nur auf Arthropoden, sondern auch auf Vertebraten zu. Die Frage ist, wie man die Entwicklungsstadien einer Art den entsprechenden Stadien anderer Arten zuordnet. Wir nehmen jetzt diese Art von Information, um im Vergleich der verschiedenen Arten die Embryonalstadien miteinander in Einklang zu bringen. Es wird praktisch morphologische Information mit der Genexpression verknüpft. Wenn du jetzt die Principal Component 3, die die Embryonalzeit in der Entwicklung widergibt, über die Entwicklungsstadien aufdröselst, siehst du hier eine mehr oder weniger lineare Beziehung. Es gibt noch eine kleine Ungenauigkeit, aber insgesamt scheint das Verhältnis zwischen der Genexpression, die zeitlich variiert, und dem morphologisch definierten Embryonalstadium ein ziemlich gutes zu sein. Das ist der Stand im Moment. Wir haben die Gene identifiziert, die auf diese Principal Component hier laden, und können jetzt weitere Analysen machen. Der Trick mit diesen Netzwerkanalysen ist, dass wir ziemlich grosse Stichproben brauchen, mindestens 20 oder so, damit wir diese statistisch robust machen können. Für einen Teil dieser Embryonalstadien haben wir dies jetzt erreicht. Alles, was wir verwendet haben, stammt von öffentlichen Datenbanken, und dafür war die Integration der Daten von verschiedenen Technologieplattformen notwendig.

## Qualität eines Datensatzes

(Christoph Hoffmann:) Ihr nehmt die Datensätze aus öffentlichen **Repositorien,** und damit kommen wir wieder auf die Frage des Vertrauens: Wie gut sind die heruntergeladenen Daten? Ist das in diesem Moment überhaupt eine Frage, oder werden solche Fragen erst im Nachhinein diskutiert?

(Hans Hofmann:) Nein, das ist von Anfang an eine Frage, denn wenn du schlechte Daten hast, willst du mit ihnen nicht deine Zeit verschwenden. Das beginnt mit diversen Quality Control-Analysen – das heisst, man schaut, ob die Daten überhaupt den Mindestanforderungen entsprechen. Alle anderen werden erst gar nicht in die Analyse mit einbezogen. Es gibt ein paar Datensätze, die haben wir ausgeschlossen.

(Christoph Hoffmann:) Und die Community hat sich darauf geeinigt, was die Mindestanforderungen sind?

(Hans Hofmann:) Ja, auf dieser Ebene auf jeden Fall. Das heisst nicht, dass sich alle daran halten, offensichtlich wird ja auch ***crappy data*** publiziert.

(Hans-Jörg Rheinberger:) Aber woran erkennst du die Güte eines Datensatzes? Das ist doch nicht von vornherein ausgemacht, wenn er öffentlich verfügbar wird. Ich könnte mir vorstellen, dass man die Daten erst einmal reinnimmt, und wenn sich dann herausstellt, dass sie mit dem grossen Rest nicht korrelieren, schmeisst man sie wieder raus.

(Hans Hofmann:) Das ist der zweite Schritt, den wir machen. Es kann sein, dass ein Datensatz die erste **Quality Control** passiert und sich dann herausstellt, dass ein systematischer Fehler darin ist und wir den betreffenden Datensatz ausschliessen müssen. Die erste Qualitätskontrolle prüft nur, ob es Daten sind, die mit der entsprechenden Technologie normalerweise produziert werden, die also die übliche Verteilung haben. Das kannst du vor der Analyse überprüfen. Die Qualitätskontrolle besteht immer aus mehreren Schritten. Zum Beispiel könnte es sein, dass die [Daten für Entwicklungsstadium 5](In_Silico_01_Testing_the_hourglass_01.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: PCs clearly distinguish species and embryological stages Poster Mittlere Spalte Mitte") irgendwo hier unten sind. So etwas haben wir schon gesehen. Wenn du dir das insgesamt anschaust, weisst du, dass wahrscheinlich etwas schiefgelaufen ist. Als Erstes würdest du dir das noch einmal anschauen, um zu sehen, ob du selber irgendeinen systematischen Fehler gemacht hast. Wenn das nicht der Fall ist, haben vielleicht die Forscher, die die Daten ins Repositorium gestellt haben, beim Hochladen einen Fehler gemacht. Wenn das nicht der Fall ist, ist eventuell irgendetwas mit der Messung schiefgelaufen usw. Unter Umständen kann man den Fehler finden und rausrechnen. Man muss auf jeden Fall ziemlich sorgfältig damit sein. Aber noch einmal zurück zur ***in silico* Evolution**. Wer die Entwicklung dieser künstlichen Organismen ablaufen lässt, sieht, dass dadurch ganz unterschiedliche Muster produziert werden können. Entwicklung kann stattfinden, das ist das Erste, das hat noch niemand gemacht. Wenn du jetzt so etwas wie natürliche Selektion anwendest und sagst, ich möchte ein gewisses Muster am Ende als meinen Phänotyp haben, und wenn du es dann im Computer 100.000 Mal ablaufen lässt, zeigt sich ausserdem, dass es manchmal relativ schnell zu diesen Mustern kommt und es manchmal viel länger dauert. In der tierischen und pflanzlichen Entwicklung würde man das Heterochronie nennen. Dass diese Heterochronie auch in diesen Modellorganismen zustande kommt, ist nun interessant. Anscheinend ist das also eine *emergent property*, das heisst eine Eigenschaft der Entwicklung von Organismen, die quasi automatisch zum Tragen kommt.

## Agent-Based Models

(Hans-Jörg Rheinberger:) Diese Geschichten funktionieren im Wesentlichen auf der Basis der Spieltheorie, oder sind da andere Theorien im Spiel?

(Hans Hofmann:) Ja, das sind eher [*Agent-based models*](https://de.wikipedia.org/wiki/Agentenbasierte_Modellierung "Agentenbasierte Modellierung"). Jeder Agent funktioniert praktisch wie ein simplifiziertes Gehirn, jeder Agent ist eine Zelle in dem Gehirn.

(Hans-Jörg Rheinberger:) Aber im Prinzip so, wie das zum Beispiel vor 40 Jahren Manfred Eigen und Ruthild Winkler in ihrem Buch *Das Spiel* beschrieben haben?[^5]

(Hans Hofmann:) Ja, aber es ist hier kein spieltheoretischer Ansatz. Spieltheorie wäre dafür nicht geeignet, weil sie nicht *Agent-based* ist, sondern numerische Lösungen für die ganze Population liefert.

(Hans-Jörg Rheinberger:) Aber du würfelst...

(Hans Hofmann:) Du generierst Variationen, und das kannst du durch Würfeln machen, wir nennen es hier Mutationen.

(Hans-Jörg Rheinberger:) Und was agiert, wenn du jetzt *Agent-based* sagst?

(Hans Hofmann:) Jede einzelne Zelle [hier](In_Silico_01_Testing_the_hourglass_02.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: MNBs use external inputs to develop complex «body plans» Poster rechts oben") ist ein Agent. Es gibt ganz simple Regeln. Zum Beispiel: Wenn der Nachbar so ist, dann bin ich so. Nur sind es zwei oder drei oder vier Regeln. Sie sind die gleichen wie bei einem biologischen Prozess. Du generierst Mutationen, die Mutationen generieren verschiedene Phänotypen, und auf die wirkt am Computer die Selektion ein. Du kannst es definieren. Du kannst am Computer Gott spielen, wenn du willst.

(Gabriele Gramelsberger:) Aber Mutationen, sind das Nachbarschaftsregeln? Also kannst du Mutationen über Nachbarschaftsregeln beschreiben?

(Hans Hofmann:) Könntest du, ist aber nicht so. Zufällig ändert sich irgendwo in einer Zelle etwas, und das hat Auswirkungen auf die Nachbarn.

(Christoph Hoffmann:) Ihr benutzt diesen Ansatz aber ausschliesslich, um die Herausbildung bestimmter Muster zu untersuchen.

(Hans Hofmann:) Ja, wir wollen erstens sehen, ob sich solche Muster entwickeln, und zweitens, wie sie sich entwickeln und ob langfristig auch so etwas wie ein Hourglass auftaucht oder nicht.

(Christoph Hoffmann:) Und wenn das Hourglass auftauchen würde, wäre es eventuell eine Bedingung von Entwicklung?

(Hans Hofmann:) Das wäre eine mögliche Interpretation. Wenn es so ein Hourglass geben sollte, könnten wir ausserdem die Korrelationen zwischen all diesen Genen rechnerisch ermitteln, um zu sehen, ob Raffs Hypothese von den Netzwerken, die entweder mehr oder weniger integriert sind, hier auch anwendbar wäre. Aber so weit sind wir noch nicht. Das Erste, was wir entdeckt haben, war, dass sich, wenn man Muster selektiert, diese Muster in der Evolution tatsächlich zuverlässig herausbilden. Als Zweites haben wir entdeckt, dass es Heterochronie gibt. Und als Drittes, dass sich, wenn man diese Experimente wiederholt, unter Umständen ganz ähnliche Endprodukte wieder entwickeln. Das siehst du [hier](In_Silico_01_Testing_the_hourglass_03.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: Repeated diversification replays the «tape of evolution» Poster rechts dritte Abbildung von oben"), wenn du links und rechts vergleichst. Diese Beobachtung geht auf Stephen Jay Gould zurück, «replaying the tape of life».[^6] Mit den *Markov network brains* im Artificial Life passiert das auch. Das ist der Stand der Dinge. Wir sind noch nicht so weit, zu sehen, ob es ein Hourglass gibt und wie das aussieht. Zusammengefasst: Zuerst haben wir Datensätze aus öffentlichen Repositorien heruntergeladen und massiert, das heisst so verändert, dass sie miteinander vergleichbar sind.

## Datenmassage

(Gabriele Gramelsberger:) Ihr habt die Datensätze «massiert»?

(Hans-Jörg Rheinberger:) Ah, jetzt kommen wir zur Datenmassage. Gestern waren wir beim Tanz mit den Daten.

(Hans Hofmann:) Wir haben die Daten massiert, damit sie zusammen analysiert werden können. Dann haben wir geschaut, ob wir reproduzieren können, was andere vorher gefunden haben. Wir können das nur zum Teil. Unter Umständen deshalb, weil wir einen **sehr komplexen Datensatz** haben und nur ein Teil des Datensatzes, ein Teil der Gene überhaupt relevant ist für das Muster, nach dem wir suchen. Deswegen haben wir als Nächstes die PCA gemacht und einen Satz von Genen identifiziert, die tatsächlich die Embryonalentwicklung wiedergeben. An dieser Stelle müssen wir jetzt weitermachen. Wir gleichen die Embryonalentwicklung auf der Genexpressionsebene mit der morphologischen Entwicklung ab. Das ist noch Work in Progress, muss weiter optimiert werden. Aber [hier](In_Silico_01_Testing_the_hourglass_04.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: Testing the hourglass requires embryolonic clock calibration Poster Mitte, ganz unten, links: PC3 vs embryological stage") sieht man, dass schon eine recht lineare Beziehung zwischen der Genexpression und der morphologischen Entwicklung besteht. Das Ganze haben wir mit den *Markov network brains* komplettiert, und dabei konnten wir zeigen, dass auch die sich entwickeln können. Bisher wurde das noch nie gemacht. In der Regel werden diese Modelle für andere Zwecke verwendet, nicht für entwicklungsbiologische Fragen. Dass sich hierbei so etwas wie Heterochronie als *emergent property* ergibt, war gar nicht unsere Frage. Genauso wenig, dass sich wiederholt Evolutionsprozesse finden lassen, die zu ähnlichen Resultaten führen. Insgesamt ist es ein **gemischter Ansatz**: Du hast *data parasitism*, dann, exploratorisch, den *fishing expedition*-Ansatz, und du hast *hypothesis testing* in derselben Studie, komplementiert mit dem *in silico*-Modellieransatz. Das ist repräsentativ für vieles, was jetzt in meiner Forschung und in der ganzen Art von Big Data Genom-relevanter Forschung passiert oder passieren wird. Darin steckt ein systembiologischer Ansatz, Entwicklungsbiologie, vergleichende Evolutionsbiologie.

(Hans-Jörg Rheinberger:) Von allem etwas, eine einzige Perspektive reicht also nicht. Du hast am Anfang gesagt, dass in dem Datensatz etwa 2.500 Gene repräsentiert sind?

(Hans Hofmann:) Für die wir Daten haben von jedem einzelnen Datensatz.

(Hans-Jörg Rheinberger:) Wie viele davon fallen jetzt unter PC3?

(Hans Hofmann:) Ich glaube, etwas über 200. Knappe 10 Prozent, 210 oder so. Wenn man guckt, wie viele Gene generell – als Transkriptionsfaktoren oder in anderer Funktion – in die Entwicklung eingreifen, nicht nur Mitläufer sind, sondern mit einer kausalen Rolle, dann lauten die Schätzungen in anderen Studien 10 bis 15 Prozent. Das können, aber müssen nicht die gesuchten sein. Aber von der Grössenordnung her kommt das ungefähr hin.

(Hans-Jörg Rheinberger:) Das würde passen. Du sagst, die 2.500 sind etwa zehn Prozent des gesamten Satzes von Genen. Und innerhalb dieser Zahl spiegeln sich die zehn Prozent noch einmal wieder, die entwicklungsbiologisch relevant sind?

(Hans Hofmann:) Wir haben guten Grund zur Annahme, dass diese 2.500 Gene repräsentativ sind. Da ist kein Bias drin. Es gibt alle möglichen Gründe, warum wir für die anderen Gene keine orthologen finden. Es scheint *random* zu sein. 2.500 für eine vergleichende Studie über 450 Millionen Jahre Evolution ist ziemlich gut. In einer anderen [Studie](In_Silico_04_Conserved_Transcriptomic_2019.pdf "Conserved transcriptomic profiles underpin monogamy across vertebrates") schauen wir uns Mating System Evolution an, also Sozialsysteme, da haben wir ungefähr 2.100. In dem Paper von Irie und Kuratani, in dem die Expressionsähnlichkeit für das Pharyngulastadium gezeigt worden ist, sind nur Daten für 300 oder 400 Gene berücksichtigt.[^7]

(Christoph Hoffmann:) Führst du eure Ergebnisse auf das Mehr an berücksichtigten Daten zurück?

(Hans Hofmann:) Ich bin mir nicht sicher, wie es zu erklären ist, ich weiss es nicht.

(Christoph Hoffmann:) Die Ausgangshypothese, das Hourglass, gibst du also nicht auf?

(Hans Hofmann:) Nein, ich denke, dass diese 200 Gene, die auf PC3 laden – das ist meine Hypothese jetzt –, dieses Muster hier wiedergeben. An diesem Punkt stehen wir. Wenn du die zwei Abbildungen [hier](In_Silico_01_Testing_the_hourglass_05.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: Interspecific transcriptome similarity is not robust Poster Mitte oben") vergleichst, siehst du aber, dass das, was publiziert worden ist, nicht so einfach reproduzierbar ist. Wir haben nach einer einfachen Antwort gesucht, und wir haben diverse **komplexe Antworten** gefunden. Jetzt sind wir der Meinung, dass unsere Datensätze gross genug für solche Netzwerkanalysen sind. Dann musst du aber auch erst einmal statistisch nachweisen, dass diese Analysen hinreichend robust sind, damit du die Ergebnisse auch glauben kannst.

(Gabriele Gramelsberger:) Aber deine Studien spiegeln diese Vernetzungstopologie nicht wieder, oder?

(Hans Hofmann:) Wir sind noch nicht so weit. Wir haben nur gezeigt, dass *Agent-based models* tatsächlich Entwicklungsphänomene zeigen können und einige Phänomene wiedergeben, die man auch in der Embryonalentwicklung sieht. Der nächste Schritt ist zu sehen, ob dieses Hourglass auch dort auftritt. Alles, was bis jetzt gezeigt worden ist, bildet die Grundlage dafür, überhaupt die Hypothese testen zu können. Und deswegen sage ich: Work in Progress.

## Warum *in silico*?

(Christoph Hoffmann:) Eine Nachfrage: Warum macht ihr das *in silico*?

(Hans Hofmann:) Ein Grund ist, dass wir nur eine Hypothese haben, eine H1, dass es Pleiotropie gibt und dass diese Netzwerkintegration entweder weniger stark oder stärker ist. Das ist nicht zufriedenstellend. Hier mag ich keine Experimente. In der Regel bevorzuge ich multiple Hypothesen und habe für diese dann mit *strong inference* entweder Unterstützung oder nicht. Wenn ich dann noch Organismen *in silico* habe, auf die auch Evolution einwirkt, über die ich jedoch viel mehr Kontrolle habe als über Tiere und Pflanzen – zumal ich die Sache wiederholt am Rechner ablaufen lassen kann – und wenn ich dann ähnliche Verhältnisse finde, stärkt das weiter meine Hypothese.

(Christoph Hoffmann:) Oder es wird eine neue Hypothese gefunden.

(Hans Hofmann:) Das ist das andere. Wahrscheinlich liefert uns dieser Ansatz **neue Hypothesen**, die wir mit den biologischen Daten testen können. Bisher können wir, was in der Literatur ist, zum Teil reproduzieren, zum Teil nicht. Wir können die Daten explorieren und die Komplexität reduzieren, so dass wir unter Umständen verstehen, warum wir die Literatur reproduzieren können oder warum wir das nicht können. Im Abgleich mit der morphologischen Entwicklung können wir die Daten so verbessern, dass sie biologisch repräsentativer sind. Und wir können ein unabhängiges System entwickeln, in dem auch Evolution stattfindet, um zu sehen, ob sich ähnliche Phänomene auftun. Das ist der kombinierte integrative Ansatz.

(Gabriele Gramelsberger:) Mir leuchtet eigentlich dieses Hourglass nicht ein. Woher kommt die [Variation in der Embryonalentwicklung](In_Silico_01_Testing_the_hourglass_06.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico*: Embryiogenesis Poster links oben") am linken Rand?

(Hans Hofmann:) Das ist eine gute Frage. Das kann unterschiedliche Gründe haben. Man sieht dort frühe Embryonalstadien, Achtzeller, Sechzehnzeller...

(Gabriele Gramelsberger:) Aber morphologisch sind die doch identisch.

(Hans Hofmann:) Sind sie nicht. Die schauen sehr unterschiedlich aus. Generell sind die Embryonen am Anfang sehr unterschiedlich. Dafür gibt es zwei Hauptgründe: Es könnte die natürliche Selektion auf sie eingewirkt haben, weil die Embryonen in verschiedenen Umwelten sind und die Selektion dafür gesorgt hat, dass sie optimal angepasst sind und deshalb unterschiedlich aussehen und unterschiedlich funktionieren. Oder es könnte sein, gerade im Gegenteil, dass während der frühen Embryonalentwicklung die Embryonen mehr oder weniger variieren können, ohne dass eine Selektion auf sie einwirkt. Auf beiden Wegen erhältst du **Variabilität**. Diese Variabilität ist zum Teil durch Selektion erklärbar und zum Teil durch unabhängige Evolution. Zum Beispiel der Fisch hier und diese Mausembryonen hatten vor 450 Millionen Jahren den letzten gemeinsamen Vorfahren. Seither durchlaufen sie eine unterschiedliche Evolutionsgeschichte. Allein auf Grund der unterschiedlichen Trajektorien würdest du erwarten, dass sie unterschiedlich sind, selbst wenn keine Selektion auf sie einwirkt. Aber natürlich hat auf sie Selektion eingewirkt, die zum Teil die Variation in der Morphologie erklärt. Es gibt statistische Methoden, um auseinanderzusetzen, inwieweit die Variation durch Selektion zu erklären ist, inwieweit durch Zufallsprozesse und inwieweit durch *phylogenetic constraint.* Das meint eine Komponente des Phänotyps einer Abstammungslinie, die eine anderweitig mögliche (oder sogar zu erwartende) evolutionäre Anpassung verhindert oder einschränkt. Wichtig ist jedenfalls, dass es unterschiedliche Gründe dafür gibt, warum es hier in diesem und warum es dort in jenem Ausmass variabel ist. Aber wir wollen nun wissen, warum es während des phylotypischen Stadiums sowohl morphologisch als potentiell auch molekularbiologisch über die Arten hinweg viel weniger Variation gibt. Wenn wir davon ausgehen, dass die frühen Embryonalstadien einander bei allen Arten ähneln, liegt das an **Ernst Haeckel**. Haeckel hat von Baers embryologische Beobachtungen mit seiner phylogenetischen Grundregel missinterpretiert.[^8] Die spielt aber in der Wissenschaft schon seit Jahrzehnten keine Rolle mehr.

(Hans-Jörg Rheinberger:) Vom generellen Bild her ist das ganz verschieden. Haeckels Idee ist: [Ontogenese wiederholt Phylogenese](In_Silico_02_Haeckel_Anthropogenie_1874_Tafel_IV+V.png "Formverhältnisse von Wirbeltierembryonen in einer frühen Entwicklungsphase, aus: Ernst Haeckel, Anthropogenie oder Entwickelungsgeschichte des Menschen, Leipzig 1874, Tafel 4 und 5").[^9] Das bedeutet, dass sie am Anfang ganz ähnlich sind und sich dann langsam auseinanderentwickeln. Also nicht Hourglass, sondern Aufspreitung von einem Ausgangspunkt.

(Hans Hofmann:) Deswegen habe ich gesagt, dass Haeckel von Baer missinterpretiert hat. Von Baer konnte nicht mehr viel dagegensetzen. Ende der 1860er Jahre war er ein alter Mann. Als dann Haeckels phylogenetische Grundregel diskreditiert wurde, ist von Baer gleich mit rausgeschmissen worden. Erst in den 1960er Jahren hat Friedrich Seidel von Baers Beobachtungen bestätigt, was 1983 Klaus Sander veranlasste, den Begriff phylotypisches Stadium einzuführen.[^10] Anfang der 1980er Jahre hat auch Rudy Raff gesagt: Wir sollten vielleicht wieder von Baer lesen, der hat uns ein Problem aufgeschrieben, das wir immer noch nicht gelöst haben. Raff ist ein brillanter Typ. Weiss unglaublich viel, auch über die Geschichte. Ich habe mit ihm noch nicht über diese Sachen gesprochen, es ist alles noch relativ neu. Seinen Essay hat er 1994 veröffentlicht, aber wieder ist nicht viel passiert, bis 2011 der Aufsatz von Irie und Kuratani erschienen ist.[^11] Das war der erste, und jetzt gibt es wohl drei oder vier solcher Aufsätze mit verschiedenen Ansätzen, die alle in High-Profile-Journals publiziert sind. Irie und Kuratani haben nur Vertebraten angeschaut. Mittlerweile wurden für Plathelminthen und für Arthropoden ähnliche Muster gefunden. Aber das ist alles exploratorisch und deskriptiv, es hat noch niemand irgendwelche Hypothesen getestet.

## Rearrangieren

(Christoph Hoffmann:) Noch einmal zurück zu dem Poster. Mich interessiert der Schritt [von der mittleren Grafik zur unteren](In_Silico_01_Testing_the_hourglass_07.pdf "Testing the Hourglass Model of Development *in vivo* and *in silico* Poster Mittlere Spalte"). Was passiert dort jeweils in der **Datenanalyse**? In der mittleren Grafik werden die Daten unter einer bestimmten Fragestellung ausgelesen und in einem Grafen rearrangiert.

(Hans Hofmann:) Sie werden rearrangiert, aber es wird auch die Komplexität reduziert. Wir kommen von Millionen von Datenpunkten zu, was weiss ich, ein paar Dutzend.

(Christoph Hoffmann:) Würdest du sagen, der nächste Schritt, von der Mitte nach unten, geht mit einer weiteren Umordnung einher? Oder wird die Grafik eher mit weiterer Information angereichert? Ich versuche zu verstehen, inwieweit die Daten von Schritt zu Schritt zunehmend mit Bedeutungen verknüpft werden.

(Hans Hofmann:) Die reduzierten Daten werden jetzt mit dem anderen Datensatz verglichen. Da findet wieder ein **Informationszuwachs** statt.

(Christoph Hoffmann:) Könnte man sagen, dass mit diesem Schritt zugleich eine weitere **Theoretisierung** einhergeht?

(Hans Hofmann:) Statt Theorie würden wir wahrscheinlich Annahmen sagen. Die Annahme ist, dass du die morphologischen Entwicklungsschritte – die, wenn du die Arten vergleichst, unter Umständen ein bisschen verschoben sind – mit den Genexpressionsdaten in Einklang bringen oder fast wortwörtlich synchronisieren kannst.

(Christoph Hoffmann:) Es entsteht eine Art Schichtung: Mit jedem Analyseschritt ist die Arbeit mit den Daten von mehr Annahmen durchsetzt. Aber ihr könnt die Schichten auch wieder abtragen und sagen: Hier hat es nicht funktioniert, gehen wir einen Schritt rückwärts. Man geht in eine Richtung und gibt sie vielleicht auf, geht wieder in eine andere Richtung...

(Hans Hofmann:) ...oder du gehst in mehrere Richtungen gleichzeitig und vergleichst dann am Ende, was die Phänomene, die du siehst, am besten erklärt.

(Christoph Hoffmann:) Die linke Kolumne des [Posters](In_Silico_01_Testing_the_hourglass.pdf "Testing the Hourglass Modell of Development *in vivo* and *in silico*") geht von einer Beobachtung und damit verknüpft von einer Hypothese aus, dem Hourglass. In der mittleren Kolumne sieht man, wie versucht wird, diese Hypothese in der Datenanalyse einzuholen. Wir sehen natürlich nur das Ergebnis. Dahinter stehen wahrscheinlich sehr viele Anläufe, die nicht signifikant sind oder nicht geeignet sind, die Ausgangsbeobachtung zu erklären respektive die Hypothese weiterzuentwickeln.

(Hans Hofmann:) Vor allem siehst du nicht, wie wir dazu gekommen sind, dass all diese Expressionsprofile, diese Datensätze, tatsächlich **miteinander vergleichbar** sind. Das ist ungefähr sechs Monate Arbeit. Eigentlich ist es ein Nebenprojekt, das mittlerweile ein Eigenleben angenommen hat. Die Undergrads lernen dabei ziemlich viel, was für Datenhandling, Analyse, Statistik usw. erforderlich ist. Die grossen Analysen werden alle bei TACC gemacht, bei *Stampede*, dem Supercomputer. Dafür braucht man *Unix* und *Shell Scripting* usw. Viele der Programme zur Analyse der Transkriptome sind in *Python* geschrieben, und die statistischen Analysen werden alle in *R* gemacht. Das ist alles **Open Source Software**. Die *Markov network brains* sind in *C++* implementiert, das ist eine Programmiersprache. Das machen PostDocs in dem Labor, mit dem wir zusammenarbeiten.

(Christoph Hoffmann:) Kannst du noch ein bisschen mehr zum zeitlichen Rahmen des Projekts sagen?

## Zeiten des Experimentierens

(Hans Hofmann:) Wir haben damit vor ungefähr 20 Monaten angefangen. Erst einmal musst du Literatur lesen. Dann musst du all die **Datensätze** identifizieren und finden, dich zum Teil mit den Originalautoren der Datensätze auseinandersetzen, um Fragen zu klären. Zum Beispiel kann es sein, dass sie nur Daten hochgeladen haben, die schon in einer gewissen Art und Weise verarbeitet wurden, während wir die Originaldaten haben wollen. Die Leute waren recht hilfreich, wir haben die Daten alle bekommen. Als wir die Microarraydaten und RNAseq-Daten hatten, mussten wir sie **kompatibel machen**. Dazu gibt es ein paar Ansätze in der Literatur, mit denen aber niemand zufrieden ist. Deshalb haben wir unseren eigenen Ansatz erfunden, was eine ganze Weile gedauert hat, am längsten wahrscheinlich bisher. Währenddessen haben die Kollegen von Michigan State mit den *Markov network brains* angefangen. Sie hatten am Anfang ebenfalls einige Probleme mit der Frage, wie sie das implementieren sollen, ohne dass das Endresultat vorherbestimmt wird, damit tatsächlich Evolution passieren kann. Du weisst ja vorher nicht, was passiert. Eine Frage war auch, welche Metriken man auslesen muss, um ihre Daten mit unseren Daten vergleichbar zu machen. Daran arbeiten wir immer noch. Die Analysen, die auf dem Poster gezeigt werden, wurden wahrscheinlich innerhalb von zwei Wochen durchgeführt, das ging recht schnell. Jetzt haben wir eine ziemlich klare Idee, was wir auf unserer Seite noch machen müssen. Wenn wir das erledigt haben, denke ich, können wir ein erstes Paper schreiben, mit dem wir ein Fundament legen. Und dann das nächste Paper, in dem wir tatsächlich die Hypothese testen.

(Christoph Hoffmann:) Hans-Jörg, wenn du an deine Laborarbeit denkst, würdest du sagen, die Zeitverteilung hat sich stark verändert? Hans sagt, die meiste Zeit hat die Aufbereitung des Rohmaterials, so würde ich es jetzt mal nennen, benötigt.

(Hans-Jörg Rheinberger:) Wenn du alles einberechnest… Du musst ja ständig Nachschub produzieren, Komponenten, die du experimentell brauchst, müssen immer wieder neu verfügbar gemacht werden, alte Chargen müssen mit neuen abgeglichen werden usw. usf. Ich würde mal sagen, 80 Prozent sind experimentelle **Infrastrukturarbeit** und 20 Prozent kannst du schlaue Experimente machen. Das ist die Grössenordnung, Pi mal Daumen. Kann von Bereich zu Bereich immer ein bisschen variieren.

(Hans Hofmann:) Wir haben Laborexperimente, die ungefähr so lange dauern: ein, zwei Jahre locker.

(Hans-Jörg Rheinberger:) Für ein substantielles Paper sind ein bis zwei Jahre Arbeit nicht viel.

(Christoph Hoffmann:) Es geht mir darum, sich klarzumachen, wie viel Zeit in die Vorbereitung fliesst und jetzt bei Hans in die Aufbereitung des Datenmaterials.

(Hans Hofmann:) Das ist schon ziemlich der Grossteil.

(Hannes Rickli:) Und wie verläuft nun die Unterscheidung zwischen Vorbereitung der Infrastruktur und der kurzen experimentellen Zeit? Hans, wie ist das bei dir: Die Daten abzugleichen ist ein Infrastrukturprojekt?

(Hans Hofmann:) Ja, das ist eine Voraussetzung dafür, überhaupt die Analysen machen zu können.

(Hannes Rickli:) Der experimentelle Teil beginnt also mit der Datenanalyse?

(Hans Hofmann:) Das ist der Teil, in dem du die Fragen stellst.

(Hans-Jörg Rheinberger:) Sagen wir mal, du planst ein Experiment. Du weisst so ungefähr, was du machen willst und was du brauchst. Wenn du das jetzt in Wochentage umrechnest, würde ich sagen, du bereitest es von Montag bis Donnerstag vor und am Freitag gehst du dran. Es ist viel Routine dabei.

(Hans Hofmann:) Mit den Daten, die wir massiert haben usw., können wir allerdings noch alle möglichen anderen Studien durchführen.

(Christoph Hoffmann:) Aber essentiell ist, dass die Daten aus den Repositorien für deine Forschung homogenisiert wurden. Das steckt jetzt in den Daten drin. Natürlich ist das keine grosse Angelegenheit, es ist einfach für die Aufgabe notwendig.

(Hans Hofmann:) Aber es ist nicht trivial.

(Christoph Hoffmann:) Nein, es ist nicht trivial. Es zeigt, dass Daten nicht einfach zur Verfügung stehen, sondern jeweils aufwändig aufbereitet werden müssen.

(Hans Hofmann:) Auf dem Poster ist das überhaupt nicht zu sehen.

(Gabriele Gramelsberger:) Du schaffst einen **Datenorganismus**, wie einen Modellorganismus, und darauf kannst du dann deine Experimente laufen lassen.

(Hans Hofmann:) Ich könnte dir ein anderes [Poster](In_Silico_03_Bhamidipati_Bioinformatic_pipeline_2016.pdf "A Bioinformatic Pipeline to Test the Hourglass Model of Vertebrate Development") zeigen, auf dem zu sehen ist, wie diese Ablage stattfindet. Wir haben uns auch schon überlegt, ob wir dazu ein Methodenpaper publizieren sollen. Wahrscheinlich machen wir das nicht, sondern bringen die Methode einfach in dem anderen Paper. Es ist relevant, aber nicht so ein grosser Fortschritt.

(Christoph Hoffmann:) Das ist ein sehr interessanter Punkt, an dem unsere verschiedenen Perspektiven deutlich werden. Für dich ist es ein methodisches Problem, das du lösen musst, um valide Ergebnisse zu erhalten.

(Hans Hofmann:) Zuerst einmal, um das machen zu können, was ich will.

(Christoph Hoffmann:) Und für mich steht dieser Schritt für sich im Zentrum, weil sich an ihm verstehen lässt, dass Datensätze zunächst nur ein bunter Haufen sind.

(Hans Hofmann:) Dich interessiert, wie diese Daten transformiert werden: die auf dem Poster verborgenen Schritte.

(Christoph Hoffmann:) Verborgen sind sie ja eigentlich nicht, ihr macht sie ja in aller Öffentlichkeit. Nur auf dem Poster fehlen sie, weil sie nicht direkt das Argument weiterbringen.

(Hans Hofmann:) Auf dem Poster würden sie nur vom Wesentlichen ablenken.

(Hannes Rickli:) Das finde ich schon interessant. In meinen früheren Studien zu den Videogrammen haben wir ja die **Zurichtungsverhältnisse** herausgearbeitet – den Abgleich zwischen Medien, Organismen, dem Verhalten des Teams, der Kamera, der Umwelt, den Lichtverhältnissen usw.; diese 100 Faktoren, die sich ins Bild eintragen; die ganzen Vorbereitungen, das Kalibrieren –, um dann spezifische Fragen zu stellen. Hier findet das alles vollständig im Digitalen statt. Trotzdem werden wahrscheinlich noch viel mehr Komponenten miteinander massiert, oder?

(Hans Hofmann:) Wissenschaft ist 90 Prozent Schweiss und 10 Prozent Glück. Oder man braucht Spürsinn.

(Gabriele Gramelsberger:) Aber ihr habt doch eine eigene Hypothese, um das Hourglass zu erklären?

(Hans Hofmann:) Nichts wirklich Solides. Wir haben ein paar Ideen. Das [Poster](In_Silico_03_Bhamidipati_Bioinformatic_pipeline_2016.pdf "A Bioinformatic Pipeline to Test the Hourglass Model of Vertebrate Development"), über das wir eben gesprochen haben, ist relativ simpel; ein Undergraduateposter für ein Undergraduate Research Forum. Es erklärt, was eigentlich die Sache motiviert hat: das Hourglass, die alternativen Hypothesen, Random Variation oder Haeckels Trichter. Dann zeigt das Poster die verschiedenen Datensätze, mit denen wir arbeiten: welche mit RNAseq gewonnen wurden und welche mit Microarray. Die verschiedenen Technologien werden sogar kurz erklärt. Normalerweise würde ich das nie machen, aber für ein Undergraduateposter ist es ideal, weil die Person dadurch zeigen kann, dass sie versteht, wovon sie spricht. Anschliessend wird das Problem quantitativ dargestellt. Die Daten, die durch diese Linien abgebildet werden, sitzen offensichtlich nicht aufeinander, das muss irgendwie geändert werden. Hier wird gezeigt, welche Korrektur wir anwenden und was am Ende rauskommt. Das ist die **Pipelinestrategie**, nach der die Daten gefiltert und korrigiert werden; eine relativ simple Methode. Die Mathematik, die darin steckt, bildet keine sonderliche Herausforderung. Nur musst du halt deine Kollegen überzeugen, dass es tatsächlich eine valide Methode ist.

(Christoph Hoffmann:) Muss jeder einzelne Datensatz die Pipeline durchlaufen, oder kann ich zum Beispiel zehn Datensätze aus demselben Labor und mit derselben Technologie erstellt zusammenpacken?

(Hans Hofmann:) Könnte man machen, machen wir aber nicht. Es gibt einen anderen Ansatz, in dem so etwas Ähnliches geschieht. Sagen wir, du hast einen multidimensionalen Raum und die Datensätze können überall in diesem Raum sein und du musst sie zusammenführen. Dann kann es sinnvoll sein, dass die Korrektion und Filterung, die du mit einem Datensatz machst, von den anderen Datensätzen informiert wird. Das ist auch eine ziemlich übliche Variante. In unserem Fall macht es aber keinen Unterschied, ob wir es einzeln machen oder integriert. Es macht keinen Unterschied, aber es könnte einen Unterschied machen. Ein Teil dieser Abbildungen wird später wahrscheinlich als **Supplementary Material** in dem Paper sein, um die Methode zu erklären. Am Ende wird das vielleicht nur eine Abbildung sein mit zwei Panels. Also relativ schlicht.

(Hannes Rickli:) Wie sehen die Daten aus, die ihr heruntergeladen habt? Das würde mich interessieren.

(Hans Hofmann:) Das sind einfach nur ziemlich grosse Textfiles. Verschiedene Zeilen und Spalten, die verschiedene Gene repräsentieren, und jedes Gen hat verschiedene Eigenschaften, also Expressionslevel, verschiedene Qualityflags usw. Hier auf dem Schirm vom Rechner siehst du ein solches File für die Maus, Embryonalstadium 10 und 10.5. So kommt das von den Repositorien.

(Hannes Rickli:) Und wenn du es in ein Auswerteprogramm steckst, wird daraus vernünftiger Text?

(Hans Hofmann:) Nein, es produziert wahrscheinlich nie Text, aber es verarbeitet die Daten mit verschiedenen Analyseskripten, und am Ende überführst du das Ergebnis in Abbildungen.

(Hannes Rickli:) Das ist wahrscheinlich nicht statthaft, aber ich wäre an einem Auszug aus dem Datensatz interessiert, den du eben gezeigt hast.

(Hans Hofmann:) Das ist auf jeden Fall statthaft, die Datensätze sind ja öffentlich verfügbar. Du kannst es selber runterladen.

[^1]: Karl Ernst von Baer, [Über Entwickelungsgeschichte der Thiere](https://archive.org/details/berentwickelun01baer "Über Entwickelungsgeschichte der Thiere"). Beobachtung und Reflexion, 2 Bde. Königsberg 1828/1837.

[^2]: Alex T. Kalinka, Karolina M. Varga, Dave T. Gerrard, Stephan Preibisch, David L. Corcoran, Julia Jarrells, Uwe Ohler, Casey M. Bergman, Pavel Tomancak, [Gene expression divergence recapitulates the developmental hourglass model](https://www.nature.com/articles/nature09634), in: Nature 468, 2010, 811-814; Tomislav Domazet-Lošo, Diethard Tautz, [A phylogenetically based transcriptome age index mirrors ontogenetic divergence patterns](https://www.nature.com/articles/nature09632), in: Nature 468, 2010, 815-818; Naoki Irie, Shigeru Kuratani, [Comparative transcriptome analysis reveals vertebrate phylotypic period during organogenesis](https://www.nature.com/articles/ncomms1248), in: Nature Communications 2, 2011, Artikel 248; Itai Yanai, Leonid Peshkin, Paul Jorgensen, Marc W. Kirschner, [Mapping gene expression in two *Xenopus* species: evolutionary constraints and developmental flexibility](https://www.sciencedirect.com/science/article/pii/S1534580711001213), in: Developmental Cell 20, 2011, 483-496; Alex T. Kalinka, Pavel Tomancak, [The evolution of early animal embryos: conservation or divergence?](https://www.cell.com/trends/ecology-evolution/fulltext/S0169-5347(12)00072-9), in: Trends in Ecology & Evolution 27, 2012, 385-393; Marcel Quint, Hajk-Georg Drost, Alexander Gabel, Kristian Karsten Ullrich, Markus Bön, Ivo Grosse, [A transcriptomic hourglass in plant embryogenesis](https://www.nature.com/articles/nature11394), in: Nature 490, 2012, 98-101; Naoki Irie, Shigeru Kuratani, [The developmental hourglass model: a predictor of the basic body plan?](http://dev.biologists.org/content/141/24/4649), in: Development 141, 2014, 4649-4655.

[^3]: Klaus Sander, The evolution of patterning mechanisms: gleanings from insect embryogenesis and spermatogenesis, in: Brian C. Goodwin, Nigel Holder, Christopher Craig Wylie (Hg.), Development and evolution, Cambridge 1983, 137-159.

[^4]: Rudolph A. Raff, Developmental mechanisms in the evolution of animal form: origins and evolvability of body plans, in: Stefan Bengston (Hg.), Early life on earth, New York 1994, 489-500; Rudolph A. Raff, The shape of life. Genes, development, and the evolution of animal form, Chicago 1996. Duboule hatte eine ähnliche Idee: Denis Duboule, [Temporal colinearity and the phylotypic progression: a basis for the stability of a vertebrate Bauplan and the evolution of morphologies through heterochrony](http://dev.biologists.org/content/1994/Supplement/135), in: Development 1994 (Supplement), 135-142.

[^5]: Manfred Eigen, Ruthild Winkler, Das Spiel. Naturgesetze steuern den Zufall, München 1975.

[^6]: Stephen Jay Gould, Zufall Mensch. Das Wunder des Lebens als Spiel der Natur (1990), übers. von Friedrich Griese, München 1994.

[^7]: Irie, Kuratani, [Comparative transcriptome analysis reveals vertebrate phylotypic period during organogenesis](https://www.nature.com/articles/ncomms1248) (Fn. 2).

[^8]: Søren Løvtrup, [On von Baerian and Haeckelian recapitulation](https://academic.oup.com/sysbio/article-abstract/27/3/348/1640354?redirectedFrom=fulltext), in: Systematic Zoology 27, 1978, 348-352.

[^9]: Ernst Haeckel, [Allgemeine Entwickelungsgeschichte der Organismen. Kritische Grundzüge der mechanischen Wissenschaft von den entstehenden Formen der Organismen begründet durch die Descendenz-Theorie](https://archive.org/details/generellemorphol02haec) (Generelle Morphologie der Organismen, Bd. 2), Berlin 1866, 300.

[^10]: Friedrich Seidel, Körpergrundgestalt und Keimstruktur. Eine Erörterung über die Grundlagen der vergleichenden und experimentellen Embryologie und deren Gültigkeit by phylogenetischen Überlegungen, in: Zoologischer Anzeiger 164, 1960, 245-305; Sander, The evolution of patterning mechanisms (Fn. 3).

[^11]: Raff, Developmental mechanisms in the evolution of animal form (Fn. 4); Irie, Kuratani, [Comparative transcriptome analysis reveals vertebrate phylotypic period during organogenesis](https://www.nature.com/articles/ncomms1248) (Fn. 2).

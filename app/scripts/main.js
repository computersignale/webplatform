// ================================
// Project: Computersignale ZHdK
// Author: Jan Huggenberg
// Date: 17.01.2019
// Remarks: 
// - Main Buildorder: initApp() > navigateToCurrentURL() > buildPage()
// - The state object is sent with every page build and history entry.
// The object contains information about the current page and article
// (if present) for more information check doc/general.md

// ==============================
// global variables
var pages;  // to store pages.json information
let stringList; //to store all translation strings
var currentPage;
let mediaPath = "/media/articles/"; // path to article media files
var article = {
    title: "",      // title of the article
    view: "meta",   // current view (meta, read, outline)
    chars: 0,       // total character count
    posPct: 0,      // current position in percentage
    posChar: 0,     // current position in characters
    hash: "",       // current position hashtag (#anchor)
    rawText: "",    // raw markdown text
    collections: [] // media collections within the article
};
var converter = new showdown.Converter();   // markdown converter
let searchIndex;
if (sessionStorage.getItem("lang") == null) // set default language
    sessionStorage.setItem("lang", "de");
let flexPages = [
    "/home",
    "/rigi/intro"];


// ==============================
// run app
initApp();


// ==============================
// main
function initApp() {

    // all user events are handled within this function.
    registerEventListeners();

    // fetch json files (Fetch API)
    var promises = [];
    promises.push( fetch('/pages.json').then(response => response.json()) );
    promises.push( fetch('/strings.json').then(response => response.json()) );

    // handle responses
    Promise.all(promises)
        .then(function(json) {
            pages = json[0].pages; // save pages to global variable
            stringList = json[1].strings;  // save translations to global variable

            // read search index file
            fetch('/searchIndex.json')
                .then(response => { return response.json(); })
                .then(json => {
                    searchIndex = elasticlunr.Index.load(json);

                    // uncomment this for quick search testing
                    // setTimeout(function() {
                    //     toggleOverlay("search");
                    //     setTimeout(function() {
                    //         searchPage("daten");
                    //     }, 50);
                    // }, 10);
                });

            //navigate to URL
            navigateToCurrentURL();
        });
}


// ==============================
// navigate to current url
function navigateToCurrentURL() {

    // split url parameters into parts 
    // and create state object

    // read route from url
    var urlParts = window.location.pathname.split("/");

    // valid routes
    let validRoutes = [];
    for (var i = 0; i < pages.length; i++)
        validRoutes.push(pages[i].route);

    // determine language from URL
    if (["de", "en"].includes(urlParts[1]))
        sessionStorage.setItem('lang', urlParts[1]);

    // combine remaining parts to string
    let urlRemaining = "";
    for (let i=2; i < urlParts.length; i++)
        urlRemaining += "/"+urlParts[i];

    // check if string matches a route
    let pageRoute = "/home";
    if (validRoutes.includes(urlRemaining))
        pageRoute = urlRemaining;

    // initialize state object
    let stateObj = {route: pageRoute};

    // handle url search parameters
    let parsedUrl = new URL(window.location.href);
    if (parsedUrl.search || parsedUrl.hash) {   // url contains search parameters or hash

        if (pageRoute == "/data/cichlid/panorama") {    // texas panorama page ?loc=chillingstation&time=12:00&d=s&a1=0&a2=0&a3=0
            stateObj.panorama = {};

            for (var p of parsedUrl.searchParams)   // check parameters
                stateObj.panorama[p[0]] = p[1];

        } else if (pageRoute.startsWith("/rigi/")) {  // article page
            stateObj.article = {};

            if (parsedUrl.hash) //check hash
                stateObj.article.hash = parsedUrl.hash;

            for (var p of parsedUrl.searchParams)   // check parameters
                stateObj.article[p[0]] = p[1];
        }
    }

    buildPage(stateObj);
}


// ==============================
// build page
function buildPage(stateObj, pushHistory = true) {

    // get route
    pageRoute = stateObj.route;

    // reset page
    overlay.classList.add("hidden");
    document.getElementById('page').classList.remove("in-background");
    document.getElementById('app').classList.remove('hide-header');
    var nodes = document.querySelectorAll('#app__header .button');
    for (node of nodes)
        node.classList.remove("active");

    // fetch matching page object
    for (page of pages)
        if (page.route == pageRoute)
            currentPage = page;

    // save parameters to article object
    if (stateObj.article != undefined){
        article.route = currentPage.route;
        article.view = stateObj.article.view;
        article.posPct = stateObj.article.pct;
        article.posChar = stateObj.article.char;
        article.hash = stateObj.article.hash;
    } else // default
        article.view = 'meta';

    // push state to browser history
    if (pushHistory){
        var url = "/"+sessionStorage.getItem('lang')+stateObj.route;

        // generate url search parameters for articles
        if (stateObj.article != undefined){
            url += '?view='+article.view;

            if (stateObj.article.pct != undefined)
                url +='&pct='+article.posPct;

            if (stateObj.article.char != undefined)
                url +='&char='+article.posChar;

            if (stateObj.article.view == 'read')
                url += article.hash;
        }

        // generate url search parameters for panorama
        if (stateObj.panorama != undefined){
            url += '?loc='+stateObj.panorama.loc;

            if (stateObj.panorama.time != undefined)
                url +='&time='+stateObj.panorama.time;

            if (stateObj.panorama.display != undefined)
                url +='&display='+stateObj.panorama.display;

            if (stateObj.panorama.states != undefined)
                url +='&states='+stateObj.panorama.states;

            if (stateObj.panorama.play != undefined)
                url +='&play='+stateObj.panorama.play;
        }

        window.history.pushState(stateObj, '', url);
    }

    // set page title
    document.title =stringList["computer-signals"][sessionStorage.getItem("lang")]+' - '+currentPage.title[sessionStorage.getItem("lang")];

    // set meta data
    document.getElementsByTagName("html")[0].setAttribute("lang", sessionStorage.getItem("lang"));
    if (sessionStorage.getItem("lang") == "de") {
        document.querySelector('meta[name="description"]').setAttribute("content", "Computersignale - Kunst und Biologie im Zeitalter ihres digitalen Experimentierens");
    } else {
        document.querySelector('meta[name="description"]').setAttribute("content", "Computer Signals - Art and Biology in the Age of Digital Experimentation");
    }

    // display app body as flexbox (scale to full height of viewport)
    // currently used for home page and article read mode only
    // to automatically render new article pages as flexbox
    if (flexPages.includes(currentPage.route) || 
        article.view == "read")
        document.querySelector("[id^=app__body]").id = "app__body--flex";
    else
        document.querySelector("[id^=app__body]").id = "app__body";


    // ---------------
    // load page content
    // is static page
    if (currentPage.content.endsWith(".html")){
        loadFile('/components/contents/'+sessionStorage.getItem("lang")+'-'+currentPage.content)
        .then(function(response) {
            document.getElementById("page").innerHTML = response;   // replace page content
            pageLoaded();   // things to do after the page has been loaded completely
        });

    // ---------------
    // is article page
    } else if (currentPage.content.endsWith(".md")) {
        buildArticlePage(stateObj);

    // ---------------
    // run function (currently for Soundscape only)
    } else if (currentPage.content.endsWith("()")) {
        // run javascript function
        eval(currentPage.content);
        pageLoaded();    // things to do after the page has been loaded completely
    }
}


// ==============================
// build article page
function buildArticlePage(stateObj) {

    // clear page
    var page = document.getElementById("page");
    document.getElementById('app').classList.remove('hide-header');
    page.innerHTML = "";

    // reset article object
    if (article.route != currentPage.route){
        article = {
            route: currentPage.route,
            view: "meta",
            chars: 0,
            posPct: 0,
            posChar: 0,
            hash: "",
            rawText: "",
            collections: [],
            size: []
        };
    }


    // ==============================
    // register mutation observer to trigger 
    // stuff after article text has been added to the DOM
    let targetNode = document.getElementById("app");

    // Options for the observer (which mutations to observe)
    let config = { childList: true, subtree: true };

    // Callback function to execute stuff after text has been loaded
    let onMutation = function(mutationsList) {
        for (let mutation of mutationsList) {
            for (let node of mutation.addedNodes) {
                if (node.nodeType === 1){
                    if (node.matches('[id*="page__body"]')) {

                        // read view
                        if (article.view == "read") {
                            setTimeout(prepareArticlePage());

                        // outline view
                        } else if (article.view == "outline") {

                            repositionOutlineItems();
                            drawOutline();

                            // jump to location
                            // 1. priority: hashtag
                            if (article.hash != undefined && article.hash != ''){
                                let target = document.getElementById(article.hash.replace('#', ''));
                                target.scrollIntoView();
                            }

                            // 2. priority: percentage
                            else {
                                let height = document.querySelector('.list--outline').offsetHeight;
                                let pixels = Math.round(height * (article.posPct/100));
                                // disabled autoscroll in outline view because it seems to confuse
                                // users more then it helps
                                // document.getElementById('app__body').scrollTop = pixels;
                            }

                            drawMinimap();
                        }

                        pageLoaded();

                        // disconnect observer after page load
                        observer.disconnect();
                    }
                }
            }
        }
    };

    // Create an observer instance linked to the callback function
    let observer = new MutationObserver(onMutation);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);


    // ==============================
    // load base DOM and article file
    var promises = [];
    promises.push(loadFile('/components/contents/'+sessionStorage.getItem("lang")+'-content-rigi-base.html'));
    promises.push(loadFile('/components/articles/'+sessionStorage.getItem("lang")+'-'+currentPage.content));
    page.innerHTML = `<div id="loader">
            <div class="spinner"></div>
            <div class="message">`+stringList["page-loading"][sessionStorage.getItem("lang")]+`</div>
        </div>`;

    // handle contents
    Promise.all(promises)
    .then(function(response) {

        var baseHTML = response[0];
        var markdown = response[1];

        // save for later
        article.rawText = markdown;

        // extract collections from text
        let string = article.rawText;
        let re = /\[[^\]]+\]\([^\)]+\)(\[\]\([^\)]+\))+/g;
        let collectionsStr = string.match(re);

        if(collectionsStr)
            for (let collectionStr of collectionsStr){

                let re = /\(([^\"]+) \"([^\"]+)\"\)/g;
                let itemsStr = re.exec(collectionStr);
                let collection = [];

                while (itemsStr != null) {
                    let item = {    // new collection item
                            path: mediaPath+itemsStr[1],    // path to media
                            caption: itemsStr[2]    // caption of media
                    }
                    collection.push(item);
                    itemsStr = re.exec(collectionStr);
                }
                article.collections.push(collection);
            }

        // insert base html
        var base = document.createElement('div');
        base.innerHTML = baseHTML;

        // general modifications of base
        base.querySelector('.title').innerHTML = currentPage.title[sessionStorage.getItem("lang")];
        base.querySelector('.title').href = currentPage.route+"?view=meta";
        if (article.posPct == "undefined"|| 
            article.posPct == undefined ||
            article.posPct == 0) {
            base.querySelector('.view-meta').href = currentPage.route+"?view=meta";
            base.querySelector('.view-read').href = currentPage.route+"?view=read";
            base.querySelector('.view-outline').href = currentPage.route+"?view=outline";
        } else {
            base.querySelector('.view-meta').href = currentPage.route+"?view=meta&pct="+article.posPct;
            base.querySelector('.view-read').href = currentPage.route+"?view=read&pct="+article.posPct;
            base.querySelector('.view-outline').href = currentPage.route+"?view=outline&pct="+article.posPct;
        }

        // The intro by Hans-Jörg Rheinberger is an exception and
        // only has a read view. Meta and Outline are hidden.
        if (currentPage.route == "/rigi/intro") {
            article.view = "read";
            let cells = base.querySelectorAll('.views li'); 
            for (var i = 0; i < 3; i++)
                cells[i].remove();
        }

        // modify base layout
        // ---------------
        // build read layout
        if (article.view == "read") {
            if (base.querySelector('.views .view-read'))
                base.querySelector('.views .view-read').classList.add("active");

            // process text
            var html = processText(markdown, article.view);
            base.querySelector('#article__text').innerHTML = html;


        // ---------------
        // build outline layout
        } else if (article.view == "outline") {
            base.querySelector('#page__body--pageflip').id = "page__body--outline";
            base.querySelector('.views .view-outline').classList.add("active");
            base.querySelector('.icon-fullscreen').remove();
            base.querySelector('#article__text').remove();
            base.querySelector('#list--prev-next').remove();

            // add outline container
            let outlineContainer = document.createElement('div');
            outlineContainer.id = 'outline-container';
            // process text
            let html = processText(markdown, article.view);
            outlineContainer.innerHTML = html;
            base.querySelector('#page__body--outline').appendChild(outlineContainer);

            // add minimap container
            let minimapContainer = document.createElement('div');
            minimapContainer.id = 'minimap-container';
            base.querySelector('#page__body--outline').appendChild(minimapContainer);

            // this is the fixed, dynamic element with all the minimap contents
            let wrapper = document.createElement("div");
            wrapper.id = "minimap-wrapper";
            minimapContainer.appendChild(wrapper);

            // adding article quick links
            let articlesContainer = document.createElement("div");
            articlesContainer.id = "articles";
            wrapper.appendChild(articlesContainer);

            let articles = [];

            let article1 = document.createElement("a");
            article1.innerHTML = stringList["1-data"][sessionStorage.getItem("lang")];
            article1.href="/rigi/data?view=outline";
            articles.push(article1);

            let article2 = document.createElement("a");
            article2.innerHTML = stringList["2-software"][sessionStorage.getItem("lang")];
            article2.href="/rigi/software?view=outline";
            articles.push(article2);

            let article3 = document.createElement("a");
            article3.innerHTML = stringList["3-infrastructures"][sessionStorage.getItem("lang")];
            article3.href="/rigi/infrastructures?view=outline";
            articles.push(article3);

            let article4 = document.createElement("a");
            article4.innerHTML = stringList["4-in-silico"][sessionStorage.getItem("lang")];
            article4.href="/rigi/in-silico?view=outline";
            articles.push(article4);

            for (let article of articles) {
                article.classList.add("article", "link--page");
                articlesContainer.appendChild(article);

                if(article.text == currentPage.title[sessionStorage.getItem("lang")])
                    article.setAttribute("data-state", "active");
            }


        // ---------------
        // build meta layout (default)
        } else {
            base.querySelector('.views .view-meta').classList.add("active");

            // modify base
            base.querySelector('.icon-fullscreen').classList.add("removed");
            base.querySelector('#page__body--pageflip').id = "page__body";
            base.querySelector('#article__text').classList.add("removed");
            base.querySelector('#list--prev-next').classList.add("removed");

            // process text
            var html = processText(markdown, article.view);
            base.querySelector('#page__body').innerHTML = html;
        }

        // ---------------
        // render modified base to screen
        page.innerHTML = base.innerHTML;
    });
}


// ==============================
// process text and return html
function processText(markdown, view) {
    var md = markdown;
    var parts = md.split("---\n---\n\n");
    var html;

    // process read text
    if (view == "read") {
        md = parts[1];
        md = md.split('[^1]: ')[0]; // remove footnotes
        md = md.replace(/\((.+:)\)/g, '$1'); // remove speakers name
        md = md.replace(/\[\^([0-9]*)\]/g, '<a href="^$1"><sup>[$1]</sup></a>'); // replace footnotes with html
        html = converter.makeHtml(md);

        var node = document.createElement('div');
        node.innerHTML = html;

        // add class to links and remove collection-links
        var links = node.querySelectorAll("a");
        let lastLink;
        let collectionId = 0;
        links.forEach(function(link) {
            if (link.innerHTML) {
                link.classList.add('link--media');
                lastLink = link;
                let type = getMediaType(link.getAttribute('href'));

                //add mediaPath to local media links
                if ( type == "audio" ||
                     type == "video" ||
                     type == "image" ||
                     type == "pdf" )
                    link.setAttribute('href', mediaPath+link.getAttribute('href'));

                else if (type == "internal")
                    link.setAttribute('href', "/"+sessionStorage["lang"]+link.getAttribute('href'));
                    // link.classList.replace("link--media", "link--page");

            // remove empty links (these are part of link collections)
            // and mark first link as collection
            } else {
                // label as collection and save collection ID
                if (!lastLink.classList.contains('collection')){
                    lastLink.classList.add('collection');
                    lastLink.setAttribute("data-collection", collectionId);
                    collectionId++;
                }
                // remove additional collection links
                link.remove();
            }
        });

        // number targetable nodes with ID
        var links = node.querySelectorAll("a, strong");
        var index = 0;
        links.forEach(function(link) {
            // exclude footnote
            if(!(link.nodeName == 'A' && link.getAttribute('href').startsWith("^"))){
                link.id = "link-"+index;
                index++;
            }
        });

        html = node.innerHTML;


    // process outline text
    } else if (view == "outline") {

        md = parts[1];
        md = md.split('[^1]: ')[0]; // remove footnotes
        html = converter.makeHtml(md);

        // create reference node
        var node = document.createElement('div');
        node.innerHTML = html;

        // handle collection-links
        // remove following links and label first item
        let links = node.querySelectorAll("a");
        let lastLink;
        let collectionId = 0;
        links.forEach(function(link) {
            if (link.innerHTML){
                lastLink = link;
            } else {
                // label as collection and save collection ID
                if (!lastLink.classList.contains('collection')){
                    lastLink.classList.add('collection');
                    lastLink.setAttribute("data-collection", collectionId);
                    collectionId++;
                }
                // remove additional collection links
                link.remove();
            }
        });

        // create node iterator
        var nodeIterator = document.createNodeIterator(
            node,
            NodeFilter.SHOW_ELEMENT,
            { acceptNode: function(node) {
                if (/^(h1|h2|a|strong)$/.test(node.nodeName.toLowerCase())) {
                    return NodeFilter.FILTER_ACCEPT;
                }
            }},
            false
        );

        // insert unsorted list
        var list = document.createElement("ul");
        list.classList.add("list--outline");
        list.style.height = html.length / 7+"px";  // define relative height for the container


        // insert chapter title
        let chapterTitle = document.createElement("li");
        chapterTitle.classList.add("chapter-title", "chapter");
        chapterTitle.style.top = "0%";
        let chapterLink = document.createElement("a");
        chapterLink.innerHTML = currentPage.title[sessionStorage.getItem("lang")];
        chapterLink.href = currentPage.route;
        chapterLink.classList.add("link--page");
        chapterTitle.appendChild(chapterLink);
        list.appendChild(chapterTitle);


        let images = [];
        const addImage = path => {
            if (!images.includes(path))
                images.push(path);
        };

        // insert filtered nodes as list elements
        var currentNode;
        var i = 0;
        var j = 0;
        var id = 0;

        while (currentNode = nodeIterator.nextNode()) {
            var nodeName = currentNode.nodeName.toLowerCase();
            var listItem = document.createElement("li");
            listItem.classList.add('outline-item');
            var link = document.createElement("a");
            link.id = currentNode.id;

            // handle title node
            if (nodeName == "h1" || nodeName == "h2") {
                link.innerHTML = currentNode.innerHTML;
                link.setAttribute("href", currentPage.route+"?view=read#"+currentNode.getAttribute("id"));
                link.id = currentNode.getAttribute("id");
                link.classList.add("link--page");
                listItem.classList.add("chapter");

            // handle highlight nodes
            } else if (nodeName == "strong") {
                link.innerHTML = currentNode.innerHTML;
                link.setAttribute('href', currentPage.route+"?view=read#link-"+id);
                link.classList.add("link--page");
                listItem.classList.add("highlight");
                link.id = "link-"+id;
                id++;

            // handle link nodes
            } else if (nodeName == "a") {
                link.classList = currentNode.classList; // copy over classList
                if (currentNode.getAttribute('data-collection'))
                    link.setAttribute("data-collection", currentNode.getAttribute('data-collection'));
                link.classList.add("link--media");
                link.id = "link-"+id;
                id++;

                // get href attribute ("elem.href" does not work for footnotes)
                var href = currentNode.getAttribute("href");
                let type = getMediaType(href);

                if (link.classList.contains("collection")){
                    let count = article.collections[link.getAttribute("data-collection")].length;
                    link.innerHTML = stringList["media-collection"][sessionStorage.getItem("lang")]+" ("+count+")";
                    link.href = mediaPath+href;
                    link.classList.add("icon-collections");
                    link.setAttribute("title", currentNode.getAttribute("title"));

                    // this requires collections to have a png thumbnail with the same name as the first file
                    let firstFilePath = article.collections[link.getAttribute("data-collection")][0].path;
                    let firstFilename = firstFilePath.replace(/^.*[\\\/]/, '');
                    let name = firstFilename.replace(/\.[^/.]+$/, "")
                    let posterFilePath = mediaPath+name+".png";

                    let imageNode = document.createElement("img");
                    imageNode.src = posterFilePath;
                    link.appendChild(imageNode);

                } else if (type == "image"){
                    link.innerHTML = "<img src="+mediaPath+href+">";
                    link.setAttribute("href", mediaPath+href);
                    link.setAttribute("title", currentNode.getAttribute("title"));
                    listItem.classList.add("image");
                    addImage(mediaPath+href);

                } else if (type == "pdf"){
                    // requires a png thumb for each pdf with exact same name!
                    var thumbPath = href.substr(0, href.lastIndexOf(".")) + ".png";
                    link.innerHTML = "<img src="+mediaPath+thumbPath+">";
                    link.setAttribute("href", mediaPath+href);
                    link.setAttribute("title", currentNode.getAttribute("title"));
                    listItem.classList.add("pdf");
                    addImage(mediaPath+thumbPath);

                } else if (type == "website"){
                    link.innerHTML = currentNode.getAttribute("title");
                    link.href = href;
                    link.classList.add("icon-link");
                
                } else if (type == "audio"){
                    link.innerHTML = currentNode.getAttribute("title");
                    link.href = mediaPath+href;
                    link.classList.add("icon-audio-on");
                    listItem.classList.add("audio");

                }  else if (type == "video"){
                    var thumbPath = href.substr(0, href.lastIndexOf(".")) + ".png";
                    link.innerHTML = "<img src="+mediaPath+thumbPath+">";
                    link.href = mediaPath+href;
                    link.setAttribute("title", currentNode.getAttribute("title"));
                    listItem.classList.add("video");
                    addImage(mediaPath+thumbPath);

                } else if (type == "internal"){
                    link.innerHTML = currentNode.getAttribute("title");
                    link.href = href;
                    link.classList.remove("link--media");
                    link.classList.add("link--page");
                }
            }

            // position item
            var nodeIndex = html.indexOf(currentNode.outerHTML);
             // calc position of node in percentage and add as attribute
            var posY = Math.round((100 / html.length * nodeIndex) * 100) / 100;
            listItem.style.top = posY+"%";

            // add item
            listItem.appendChild(link);
            list.appendChild(listItem);
        }

        // wait for images to be loaded
        // asyncronous loading of images via promise object
        Promise.all(images.map(checkImage))
            .then(function(response) {
                repositionOutlineItems();
                drawOutline();
                drawMinimap();
            });

        html = list.outerHTML;


    // process abstract (default)
    } else {

        // get abstract
        md = parts[0];
        md = md.replace(/\[\^([0-9]*)\]/g, '<a href="^$1"><sup>[$1]</sup></a>');
        html = converter.makeHtml(md);

        // create reference node
        var node = document.createElement('div');
        node.innerHTML = html;

        // add media link class
        var links = node.querySelectorAll("a");
        let collectionId = 0;
        links.forEach(function(link) {
            link.classList.add('link--media');
            let type = getMediaType(link.getAttribute('href'));

            //add mediaPath to local media links
            if ( type == "audio" ||
                 type == "video" ||
                 type == "image" ||
                 type == "pdf" )
                link.setAttribute('href', mediaPath+link.getAttribute('href'));
        });

        //Add table of contents
        let tocTitle = document.createElement('h2');
        tocTitle.innerHTML = stringList["toc"][sessionStorage.getItem("lang")];
        node.appendChild(tocTitle);

        var tocList = document.createElement('ul');
        tocList.classList.add('list--toc');

        //iterate through titles of article
        const regex = /\# .+/g;
        const string = parts[1];
        let m;

        while ((m = regex.exec(string)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }

            m.forEach((match, groupIndex) => {
                var anchor = match.toLowerCase().replace(/\s+|[^a-zA-Z]/g, '');

                var tocItem = document.createElement('li');
                tocList.appendChild(tocItem);

                var tocLink = document.createElement('a');
                tocLink.classList.add('link--page');
                tocLink.setAttribute('href', currentPage.route+"?view=read#"+anchor);
                tocItem.appendChild(tocLink);

                var title = document.createElement('div');
                title.innerHTML = converter.makeHtml(match);
                tocLink.innerHTML = title.querySelector('h1').innerHTML;
            });
        }

        node.appendChild(tocList);
        html = node.innerHTML;
    }

    //convert markdown to html
    return html;
}


// ==============================
// things to do after page has loaded completely
function pageLoaded() {

    // fetch and replace dynamic strings
    let dynamicStrings = document.querySelectorAll("[data-string]");
    for (let node of dynamicStrings) {
        let key = node.getAttribute('data-string');

        // find matching key in string list
        if (stringList.hasOwnProperty(key)) {
            let translation = stringList[key][sessionStorage.getItem("lang")];
            if (stringList[key].markdown) node.innerHTML = converter.makeHtml(translation);
            else node.innerHTML = translation;
        }
    }


    // update "change language" buttons
    // get current URL
    let parsedUrl = new URL(window.location.href);
    let currentURL = parsedUrl.pathname.slice(3);
    if (parsedUrl.search) currentURL = currentURL+parsedUrl.search;
    if (parsedUrl.hash) currentURL = currentURL+parsedUrl.hash;

    // replace link with current URL
    let langLinks = document.querySelectorAll('[data-set-lang]');
    for (let link of langLinks) {
        link.setAttribute("href", currentURL);   // add article attributes to link here!
        if (link.getAttribute("data-set-lang") == sessionStorage.getItem("lang"))
            link.classList.add("active");
        else
            link.classList.remove("active");
    }


    // ugly exception handling of custom script for event registration of custom audio player 
    // in remos1 page. This is where custom scripts have to be loaded after the content has been
    // added to the page.
    if(document.querySelector("audio")){
        let players = document.getElementsByTagName("audio");

        for (let player of players) {
            let container = player.closest(".audio-container");
            let progress = container.querySelector("progress");
            let position = container.querySelector(".audio-pos");
            let duration = container.querySelector(".audio-duration");

            player.addEventListener('loadedmetadata', function() {
                progress.setAttribute('max', player.duration);
                position.innerHTML = "00:00";

                // format seconds to hours minutes etc
                var date = new Date(null);
                date.setSeconds(player.duration)
                var result = date.toISOString().substr(14, 5);
                duration.innerHTML = result;
            });

            player.addEventListener('timeupdate', function() {
                // format seconds to hours minutes etc
                var date = new Date(null);
                date.setSeconds(player.currentTime)
                var result = date.toISOString().substr(14, 5);
                position.innerHTML = result;

                progress.value = player.currentTime;
            })
        }
    }
}


// ==============================
// toggle menu overlays
function toggleOverlay(type){
    // close overlay if already visible
    if (document.querySelector("."+type+"-toggle").classList.contains('active')) {
        hideOverlay();

    // display overlay
    } else {
        var overlay = document.getElementById('overlay');
        overlay.style.display = "block";

        // specific stuff
        if (type == 'menu')
            overlay.classList.add('menu-style');

        else if (type == 'search')
            overlay.classList.remove('menu-style');

        // remove active states of buttons
        var buttons = document.querySelectorAll('#app__header .button');
        for (button of buttons)
            button.classList.remove("active");

        // add active state of menu button
        document.querySelector("."+type+"-toggle").classList.add('active');

        // load content
        fetch('/components/contents/'+sessionStorage.getItem("lang")+'-overlay-'+type+'.html').then(function (response) {
            response.text().then(function (content) {
                overlay.innerHTML = content;
                showOverlay();
            });
        });
    }
}


// ==============================
// hide overlay
function hideOverlay() {
    var overlay = document.getElementById('overlay');
    overlay.classList.add("hidden");
    overlay.innerHTML = '';
    overlay.style.display = "block";
    overlay.classList.remove('menu-style');
    document.getElementById('page').classList.remove("in-background");
    var buttons = document.querySelectorAll('#app__header .button');
    for (button of buttons)
        button.classList.remove("active");
}


// ==============================
// display overlay
function showOverlay() {
    document.getElementById('overlay').classList.remove("hidden");
    document.getElementById('page').classList.add("in-background")

    // focus search input field
    if (document.getElementById('input--search'))
        document.getElementById('input--search').focus();
}


// ==============================
// display media files
function displayMedia(path, caption, collectionIndex) {

    var overlay = document.getElementById('overlay');

    showOverlay();

    // load content
    fetch('/components/contents/'+sessionStorage.getItem("lang")+'-overlay-media.html').then(function (response) {
        response.text().then(function (text) {

            // insert default content
            overlay.innerHTML = text;

            // handle media files accroding to type
            let type = getMediaType(path);

            if (type == "image"){
                var node = document.createElement("img");
                node.setAttribute('src', path);
                document.getElementById("media-viewer__content").appendChild(node);
                document.getElementById("media-viewer__caption").innerHTML = converter.makeHtml(caption);
                document.querySelector('a.icon-download').href = path;

            } else if (type == "pdf") {
                overlay.style.display = "flex"; // to display full-height
                PDFObject.embed(path, "#media-viewer__content");
                document.getElementById("media-viewer__caption").innerHTML = converter.makeHtml(caption);
                document.querySelector('a.icon-download').href = path;

                // this is an exception for a specific file that has to link to original source
                if (path.includes("In_Silico_01_Testing_the_hourglass"))
                    document.querySelector('a.icon-download').href = "/media/articles/In_Silico_01_Testing_the_hourglass.pdf";

            } else if(type == "footnote") {
                document.querySelector('#media-viewer__controls .icon-download').classList.add('hidden');
                var number = path.replace('^', '');
                var md = article.rawText;
                var delimiter = "[^1]:";    // delimiter to find footnotes
                var footnotes = md.split(delimiter)[1]; // get all footnotes
                var footnotes = delimiter.concat(footnotes); // add delimiter
                // var footnotes = footnotes.split(/\[\^.\]:/); // make array
                var footnotes = footnotes.split(/\[\^.*[0-9]\]:/g); // make array
                var html = converter.makeHtml(footnotes[number]);   // select footnote
                var footnote = document.createElement('div');
                footnote.classList.add('footnote');
                footnote.innerHTML = html;

                // add media link class
                var links = footnote.querySelectorAll("a");
                let collectionId = 0;
                links.forEach(function(link) {
                    link.classList.add('link--media');
                    let type = getMediaType(link.getAttribute('href'));

                    //add mediaPath to local media links
                    if ( type != "website" &&
                         type != "footnote" &&
                         type != "internal") {
                        link.setAttribute('href', mediaPath+link.getAttribute('href'));
                    }

                    if (type == "internal"){
                        link.classList.add('link--page');
                        link.classList.remove('link--media');
                    }
                });

                document.getElementById("media-viewer__content").appendChild(footnote);

            } else if (type == "video") {
                var node = document.createElement("video");
                node.setAttribute('controls', true);
                node.innerHTML = `<source src="`+path+`" type="video/`+path.split('.').pop()+`">
                Your browser does not support the video tag.`;
                document.getElementById("media-viewer__content").appendChild(node);
                document.getElementById("media-viewer__caption").innerHTML = converter.makeHtml(caption);
                document.querySelector('a.icon-download').href = path;

            } else if (type == "audio") {
                // png thumbnail with same filename required!!
                let thumb = path.substr(0, path.lastIndexOf(".")) + ".png";
                let container = document.createElement("div");
                container.classList.add("audio-image");
                let image = document.createElement("img");
                image.src = thumb;
                container.appendChild(image);

                var audio = document.createElement("audio");
                audio.src = path;
                audio.setAttribute("type", "audio/"+path.split('.').pop());
                audio.setAttribute("controls", "");
                audio.setAttribute("loop", "");
                audio.innerHTML = "Your browser does not support the audio tag.";
                container.appendChild(audio);

                document.getElementById("media-viewer__content").appendChild(container);
                document.getElementById("media-viewer__caption").innerHTML = converter.makeHtml(caption);
                document.querySelector('a.icon-download').href = path;
            }

            // check if media is part of a collection
            // create collection if true
            if (collectionIndex) {

                document.getElementById("media-viewer__content").classList.add("collection");

                // find current item index
                let collection = article.collections[collectionIndex];
                let curIndex;
                for (let [index, item] of collection.entries()){
                    if (Object.values(item).indexOf(caption) > -1 &&
                        Object.values(item).indexOf(path) > -1) {
                        curIndex = index;
                    }
                }

                // handle previous and next navigation buttons
                let wrapper = document.getElementById("media-viewer__content");
                let prevIndex;
                if (curIndex > 0)
                    prevIndex = curIndex-1;

                let previousLink = document.createElement("a");
                previousLink.classList.add("media-viewer__nav", "icon-chevron-left", "disabled");
                wrapper.prepend(previousLink);

                if (prevIndex != undefined){
                    previousLink.href = article.collections[collectionIndex][prevIndex].path;
                    previousLink.title = article.collections[collectionIndex][prevIndex].caption;
                    previousLink.setAttribute("data-collection", collectionIndex);
                    previousLink.classList.add("link--media", "collection");
                    previousLink.classList.remove("disabled");
                }

                let nextIndex;
                if (curIndex < article.collections[collectionIndex].length-1)
                    nextIndex = curIndex+1;

                let nextLink = document.createElement("a");
                nextLink.classList.add("media-viewer__nav", "icon-chevron-right", "disabled");
                wrapper.appendChild(nextLink);

                if (nextIndex != undefined){
                    nextLink.href = article.collections[collectionIndex][nextIndex].path;
                    nextLink.title = article.collections[collectionIndex][nextIndex].caption;
                    nextLink.setAttribute("data-collection", collectionIndex);
                    nextLink.classList.add("link--media", "collection");
                    nextLink.classList.remove("disabled");
                }

                // handle collection position indicator
                let indicator = document.createElement("ul");
                indicator.classList.add("collection-indicator");
                for (let [index, item] of collection.entries()){
                    let item = document.createElement("li");
                    indicator.appendChild(item);
                    if (curIndex == index)
                        item.classList.add("active");
                }

                wrapper.appendChild(indicator);
            }
        });
    });
}


// ==============================
// update outline stroke in outline view
function drawOutline() {
    var parentNode = document.getElementById("page__body--outline");

    // remove existing canvas
    if (parentNode.querySelector("#canvas")){
        parentNode.querySelector("#canvas").remove();
    }

    // add canvas element (stroke)
    var canvas = document.createElement("canvas");
    canvas.id = "canvas";
    var ctx = canvas.getContext("2d");

    //set canvas size to div dimensions
    ctx.canvas.width  = document.querySelector(".list--outline").offsetWidth;
    ctx.canvas.height = document.querySelector(".list--outline").offsetHeight;

    //draw line
    ctx.beginPath();
    ctx.lineWidth = "1";
    ctx.strokeStyle = "rgba(0, 0, 0, .2)";

    var items = document.querySelectorAll(".list--outline > li");
    var index = 0;

    // iterate through list items
    for (item of items) {
        var posX = item.offsetLeft * .9;
        var posY = item.offsetTop + item.offsetHeight/2;

        // chapters have different line position
        if (item.classList.contains("chapter"))
            posX = item.offsetWidth/2;

        // draw line joining all items
        if (index == 0) {
            ctx.moveTo(posX, posY);
        } else {
            if(lastPosX < posX){
                var middleX = lastPosX + ( posX - lastPosX ) / 2;
                var middleY = lastPosY + ( posY - lastPosY ) / 2;
            } else {
                var middleX = posX + ( lastPosX - posX) / 2;
                var middleY = posY + ( lastPosY - posY ) / 2;
            }
            ctx.quadraticCurveTo(lastPosX, middleY, middleX, middleY);
            ctx.quadraticCurveTo(posX, middleY, posX, posY);
        }

        // save this item position for next iteration
        lastPosX = posX;
        lastPosY = posY;

        index++;
    }

    ctx.stroke();

    // add item indicator
    for (item of items){

        if (item.offsetLeft > 0){
            ctx.beginPath();
            ctx.arc(item.offsetLeft * .9, item.offsetTop + item.offsetHeight/2, 2, 0, 2*Math.PI);
            ctx.fill();

            ctx.beginPath();
            ctx.moveTo(item.offsetLeft * .9, item.offsetTop + item.offsetHeight/2);
            ctx.lineTo(item.offsetLeft, item.offsetTop + item.offsetHeight/2);
            ctx.stroke();
        }
    }

    // append outline to parent node
    document.getElementById("outline-container").appendChild(canvas);
}


// ==============================
//initialize minimap in outline view
function drawMinimap() {
    let referenceNode = document.querySelector(".list--outline");
    let wrapper = document.getElementById("minimap-wrapper");
    let viewportState;

    // remove existing canvas
    if (document.getElementById("minimap")) {
        viewportState = document.querySelector("#minimap__viewport").getAttribute("data-state");
        document.getElementById("minimap").remove();
    }

    // create minimap node
    let minimap = document.createElement("div");

    let page = document.getElementById("page");
    var pageStyles = window.getComputedStyle(page, null);
    let outlineStyles = window.getComputedStyle(document.getElementById("outline-container"), null);
    let height = document.getElementById("app__body").offsetHeight - 
                parseInt(pageStyles.getPropertyValue("padding-top"), 10) - 
                parseInt(pageStyles.getPropertyValue("padding-bottom"), 10) -
                document.getElementById("page__header").offsetHeight -
                parseInt(outlineStyles.getPropertyValue("margin-top"), 10) -
                document.getElementById("articles").offsetHeight; // chapter blocks


    let width = document.getElementById("minimap-container").offsetWidth;
    let factor = referenceNode.offsetHeight / height;
    minimap.id = "minimap";
    minimap.style.height = height+"px";
    minimap.style.width = width+"px";

    // add hairline canvas
    let canvas = document.createElement("canvas");
    canvas.id = "minimap-hairline";
    let ctx = canvas.getContext("2d");
    minimap.appendChild(canvas);

    // add items
    let items = document.createElement("ul");
    items.classList.add("list");
    let list = referenceNode.getElementsByTagName("li");

    for (var item of list){

        if (item.classList.contains("outline-item")){
            let mapItem = document.createElement("li");
            let left = parseInt(window.getComputedStyle(item, null).getPropertyValue("left"), 10);
            let leftPct = map(left, 0, parseInt(outlineStyles.getPropertyValue("width"), 10), 0, 100);
            mapItem.style.width = item.offsetWidth / factor +"px";
            mapItem.style.height = item.offsetHeight / factor +"px";
            mapItem.style.left = leftPct+"%";
            mapItem.style.top = item.style.top;
            items.appendChild(mapItem);


            let types = ["highlight", "chapter", "video", "image", "audio", "pdf"];

            for (let type of types){

                if(item.classList.contains(type))
                    mapItem.classList.add(type);
            }
        }
    }

    minimap.appendChild(items);

    // draw minimap viewport
    var viewport = document.createElement("div");
    viewport.id = "minimap__viewport";

    // mark as grabbed if grabbed
    if (viewportState == "grabbed")
        viewport.setAttribute("data-state", "grabbed");


    // set viewport height
    var visibleHeight = document.getElementById("app__body").offsetHeight -
                        document.getElementById("page__header").offsetHeight -
                        parseInt(window.getComputedStyle(document.getElementById("page"), null).getPropertyValue("padding-top"));

    var viewportHeight = Math.min(Math.max((visibleHeight) / factor, 0), height);
    viewport.style.height = viewportHeight+"px";

    // set viewport position
    var offsetTop = Math.round(document.getElementById("app__body").scrollTop / factor);
    viewport.style.top = offsetTop+"px";

    minimap.appendChild(viewport);

    // append minimap to parent node
    wrapper.appendChild(minimap);

    // draw hairline
    ctx.canvas.width  = minimap.offsetWidth;
    ctx.canvas.height = minimap.offsetHeight;
    let canvasStyles = window.getComputedStyle(document.getElementById("minimap-hairline"), null);
    let xPos = map(.25, 0, 1, 0, parseInt(canvasStyles.getPropertyValue("width"), 10));
    ctx.lineWidth = ".4";
    ctx.strokeStyle = "rgba(0, 0, 0, .4)";
    ctx.beginPath();    
    ctx.moveTo(xPos, 0);
    ctx.lineTo(xPos, height);
    ctx.stroke();
}


// ==============================
// reposition overlapping outline items in outline view
function repositionOutlineItems(){
    // reposition overlaying items
    var items = document.querySelectorAll('.list--outline li');
    var containerHeight = document.querySelector('.list--outline').offsetHeight;
    var lastEndY = 0.0;

    for (item of items){
        var posY = parseFloat(item.style.top);
        var height =  100 / containerHeight * item.offsetHeight;
        var endY = posY + height;

        // if overlapping offset element
        var marginPx = 10;
        var margin = 100 / containerHeight * marginPx;

        if (posY < lastEndY+margin) {
            var offset = lastEndY - posY + margin;
            posY = posY + offset;
            item.style.top = posY +"%";
        }

        lastEndY = posY + height;
    }
}


// ==============================
// prepare page (sanitize and jump to position)
function prepareArticlePage() {
    sanitizeColumnCount();

    if (article.hash)
        jumpToAnchor(article.hash);
    else if (article.posChar)
        jumpToChar(article.posChar);
    else
        jumpToPct(article.posPct);
}


// ==============================
// sanitize column count to make sure text is made of even columns
function sanitizeColumnCount() {
    let element = document.getElementById("article__text");
    let className = "new-column";

    // remove existing new-column elements
    let list = element.getElementsByClassName(className);
    for (item of list)
        item.remove();

    // check if number of columns is correct
    let style = window.getComputedStyle(element);
    let gapWidth = parseInt(style.getPropertyValue("--article-column-gap"), 10);
    let cssColumnWidth = parseInt(style.getPropertyValue("column-width"));
    let frameWidth = element.clientWidth;
    let actualColumnWidth = getActualColumnWidth();
    let columnsPerPage = Math.round((frameWidth + gapWidth) / (actualColumnWidth + gapWidth));

    function getActualColumnWidth() {
        // to get the actual column with we have to reproduce how "column-width: auto" is calculating the size
        const calc = columns =>
            Math.round((frameWidth - gapWidth * (columns-1)) / columns); 
        // 1 column
        if (frameWidth <= cssColumnWidth * 2 + gapWidth * 1)
            return calc(1);
        // 2 columns
        else if (frameWidth <= cssColumnWidth * 3 + gapWidth * 2)
            return calc(2);
        // 3 columns
        else
            return calc(3);
    }

    // if multiple columns
    if (columnsPerPage > 1) {
        let columnsAdded = 0;
        let limit = 2;  // to avoid crash on browser that do not support "break-before"
        let columnCount = Math.round((element.scrollWidth + gapWidth) / (actualColumnWidth + gapWidth));
        while ( columnCount % columnsPerPage && 
                columnsAdded < limit ) {
            var node = document.createElement("div");
            node.classList.add(className);
            element.appendChild(node);
            columnCount = Math.round((element.scrollWidth + gapWidth) / (actualColumnWidth + gapWidth));
            columnsAdded++;
        }
    }
}


// ==============================
// map character to pixels
function jumpToChar(targetChar = 0) {
    if (document.getElementById("mark"))
        document.getElementById("mark").remove();
    var html = document.getElementById("article__text").innerHTML;
    var pureText = html.replace(/<(?:.|\n)*?>/gm, '');
    var targetPercentage = 100 / pureText.length * targetChar;  // not used yet

    // check if index is in valid range
    var targetChar = Math.min(Math.max(parseInt(targetChar), 0), pureText.length - 1);
    article.posChar = targetChar;

    // map index
    var htmlChar = mapCharToHTML(html, targetChar);

    // add marker
    var newHTML = html.slice(0, htmlChar) + "<span id=\"mark\"></span>" + html.slice(htmlChar);
    document.getElementById("article__text").innerHTML = newHTML;

    scrollToPx(document.getElementById("mark").offsetLeft);
}


// ==============================
// map hash to pixels
function jumpToAnchor(targetAnchor) {
    var html = document.getElementById("article__text").innerHTML;
    var element = document.getElementById(targetAnchor.replace('#', ''));
    element.classList.add('target');
    element.addEventListener("animationend", function(e){
        e.target.classList.remove("target");
    },false);

    // jump to position
    scrollToPx(element.offsetLeft);
}


// ==============================
// map page to pixels
function jumpToPage(targetPage = 0) {
    var elem = document.getElementById('article__text');
    var gap = parseInt(getComputedStyle(elem).getPropertyValue("--article-column-gap"), 10);
    var pageSize = elem.clientWidth + gap;
    var targetPx = (targetPage - 1) * pageSize;

    // delete hash
    article.hash = '';

    scrollToPx(targetPx);
}


// ==============================
// map percentage to pixels
function jumpToPct(targetPct = 0) {
    var elem = document.getElementById('article__text');
    var gap = parseInt(getComputedStyle(elem).getPropertyValue("--article-column-gap"), 10);

    // remove hash
    article.hash = '';

    var maxPx = elem.scrollWidth - elem.clientWidth;
    var targetPx = maxPx / 100 * targetPct + gap; // added gap to prevent edge-cases (maybe not clean)

    scrollToPx(targetPx);
}


// ==============================
// flip article page
function flipPage(action = "next"){
    var elem = document.getElementById('article__text');
    var gap = parseInt(getComputedStyle(elem).getPropertyValue("--article-column-gap"), 10);
    var pageSize = elem.clientWidth + gap; // page size
    var targetPx = elem.scrollLeft; // current scroll position

    // calculate scroll target
    if (action == "prev")
        targetPx -= pageSize;
    else if (action == "next")
        targetPx += pageSize;

    // page flips will always be rounded to full pages
    targetPx = Math.round(targetPx / pageSize ) * pageSize;
    article.hash = undefined;
    article.posChar = undefined;

    //scroll to position
    scrollToPx(targetPx);
}


// ==============================
// scroll to pixel
function scrollToPx(targetPx = 0) {
    var elem = document.getElementById('article__text');

    // remove element
    if (document.getElementById("selection-start"))
        document.getElementById("selection-start").remove();
    if (document.getElementById("selection-start__btn"))
        document.getElementById("selection-start__btn").remove();

    //------
    // snap to full page
    var gap = parseInt(getComputedStyle(elem).getPropertyValue("--article-column-gap"), 10);
    var pageSize = elem.clientWidth + gap;
    var contentWidth = Math.round((elem.scrollWidth + gap) / pageSize ) * pageSize; // round to nearest full page
    var maxPx = contentWidth - pageSize;
    targetPx = Math.floor(targetPx / pageSize) * pageSize;  // floor to nearest page
    targetPx = Math.min(Math.max(targetPx, 0), maxPx);  // clamp scroll target

    //------
    // update page count indicators
    var pageNumber = targetPx / pageSize + 1; // start with 1
    var pageCount = contentWidth / pageSize;
    document.getElementById('article__position').value = pageNumber;
    document.getElementById('article__pageCount').textContent = pageCount;


    // var targetPct = Math.round( 100 / Math.max(pageCount - 1, 1) * Math.max(pageNumber - 1, 1) );
    var targetPct = Math.ceil(100 / (elem.scrollWidth - elem.clientWidth) * targetPx);

    //------
    // enable or disable page flip buttons
    document.getElementsByClassName('page-prev')[0].classList.remove('disabled');
    document.getElementsByClassName('page-next')[0].classList.remove('disabled');

    if (pageNumber == 1)
        document.getElementsByClassName('page-prev')[0].classList.add('disabled');
    else if (pageNumber == pageCount)
        document.getElementsByClassName('page-next')[0].classList.add('disabled');


    // update link targets
    if (document.querySelector(".view-read")) {
        document.querySelector(".view-meta").href = currentPage.route+"?view=meta&pct="+targetPct;
        document.querySelector(".view-read").href = currentPage.route+"?view=read&pct="+targetPct;
        document.querySelector(".view-outline").href = currentPage.route+"?view=outline&pct="+targetPct;
    }

    //------
    // scroll to target
    elem.scrollLeft = targetPx;
    article.posPct = targetPct;

    // update URL without creating new history entry
    var stateObj = {
            route: currentPage.route,
            article: {
                view: article.view,
                pct: article.posPct,
                char: article.posChar,
                hash: article.hash
            }
        };

    var url = "/"+sessionStorage.getItem('lang')+stateObj.route;

    if (stateObj.article != undefined){
        url += '?view='+article.view;

        if (stateObj.article.pct != undefined)
            url +='&pct='+article.posPct;

        if (stateObj.article.char != undefined)
            url +='&char='+article.posChar;

        if (stateObj.article.view == 'read' && stateObj.article.hash != undefined)
            url += article.hash;
    }

    history.replaceState(stateObj, document.title, url);

    pageLoaded();   // to update language links
}


// ==============================
// map html target char to pure text char
function mapHTMLToChar(html, targetChar = 0) {
    let part = html.slice(0, targetChar);
    let tempHTML = document.createElement("div");
    tempHTML.innerHTML = part;
    let characterCount = tempHTML.textContent.length;
    return tempHTML.textContent.length;
}


// ==============================
// map pure text index to html index
function mapCharToHTML(html, targetChar = 0) {
    var htmlPattern = /(<(?:.|\n)*?>)/gm;   //regex pattern with capture group
    var output = [];    // array of seperated html and text elements

    // loop over the segments, including the delimiters.
    for (var token of html.split(htmlPattern))
        output.push(token);

    var i = 0;
    var pureChar = 0;
    var htmlChar = 0;

    // iterate until pureChar reaches targetChar
    while( pureChar <= targetChar ) {

        var string = output[i];
        var nextPureChar = pureChar + string.length;
        var isHTML = htmlPattern.test(string);

        if (isHTML) {   // string is html
            htmlChar += string.length;

        } else if (nextPureChar <= targetChar) {   // string is not html and does not overshoot
            htmlChar += string.length;
            pureChar += string.length;

        } else {    // string is not html but does overshoot
            var remaining = targetChar - pureChar;
            htmlChar += remaining;
            pureChar = targetChar + 1;    // set condition to stop loop
        }

        // increment
        i++;
    }

    return htmlChar;
}


// ==============================
// register event listeners on a static element
function registerEventListeners() {
    // all events are registered to the "app" element. This way events
    // targeting dynamically loaded content (via Fetch API) will always
    // work.

    // flag for minimap dragging
    let minimapDragging = false;

    // register click events
    document.getElementById("app").addEventListener("click", function(event) {
        var target = event.target;

        // copy to clipboard
        if (target.closest("#selection-start__btn")) {
            let html = document.getElementById("article__text").innerHTML;
            let characterCount = mapHTMLToChar(html, html.indexOf('<span id="selection-start">'));

            if (document.getElementById("selection-start"))
                document.getElementById("selection-start").remove();
            if (document.getElementById("selection-start__btn"))
                document.getElementById("selection-start__btn").remove();
            if (document.getElementById("mark"))
                document.getElementById("mark").remove();

            // construct precise url
            let url = window.location.protocol+"//"+window.location.hostname;

            if (window.location.port)
                url = url+":"+window.location.port;

            url = url+"/"+sessionStorage.getItem("lang")+
                    article.route+
                    "?view=read&char="+characterCount;

            // create popup window
            let shareMask = document.createElement("div");
            shareMask.id = "share-mask"
            let sharePopUp = document.createElement("div");
            sharePopUp.id = "share-mask__popup";
            shareMask.appendChild(sharePopUp);
            let citation = document.createElement("p");
            citation.innerHTML = stringList["citation"][sessionStorage.getItem("lang")];
            sharePopUp.appendChild(citation);
            let text = document.createElement("p");
            text.innerHTML = stringList["this-link-points"][sessionStorage.getItem("lang")];
            sharePopUp.appendChild(text);
            let close = document.createElement("button");
            close.classList.add("button", "icon-clear", "close");
            sharePopUp.appendChild(close);
            let wrapper = document.createElement("div");
            sharePopUp.appendChild(wrapper);
            let input = document.createElement("input");
            input.value = url;
            input.type = "text";
            wrapper.appendChild(input);
            let button = document.createElement("button");
            button.innerHTML = stringList["copy"][sessionStorage.getItem("lang")];
            button.classList.add("button", "copy");
            wrapper.appendChild(button);
            document.getElementById("page").appendChild(shareMask);
            input.select();

            button.addEventListener("click", function(event){ document.execCommand("copy");}, {capture: false});
            close.addEventListener("click", function(event){ shareMask.remove();}, {capture: false});
            shareMask.addEventListener("click", function(event){
                if (event.target.id == "share-mask")
                    shareMask.remove();
            }, {capture: false});
        }

        if (target.closest(".download"))
            window.print();

        if (target.closest(".more-text-show")){
            target.closest(".more-text-show").nextElementSibling.classList.add("active");
            target.closest(".more-text-show").remove();
        }

        if (target.closest(".more-text-toggle")){
            target.closest(".more-text-toggle").nextElementSibling.classList.toggle("active");
            target.closest(".more-text-toggle").classList.toggle("active");
        }

        if (target.closest(".audio-playall")) {
            let button = target.closest(".audio-playall");
            let players = document.getElementsByTagName("audio");
            if (button.classList.contains('icon-play')){
                for (var i = 0; i < players.length; i++) {
                    let player = players[i];
                    player.play();
                    let playerBtn = player.closest(".audio-container").querySelector(".audio-playpause");
                    playerBtn.classList.replace("icon-play", "icon-pause");
                    playerBtn.innerHTML = "Pause";
                    button.classList.replace("icon-play", "icon-pause");
                    button.innerHTML = "Pause all";
                }
            } else {
                for (var i = 0; i < players.length; i++) {
                    let player = players[i];
                    player.pause();
                    let playerBtn = player.closest(".audio-container").querySelector(".audio-playpause");
                    playerBtn.classList.replace("icon-pause", "icon-play");
                    playerBtn.innerHTML = "Play";
                    button.classList.replace("icon-pause", "icon-play");
                    button.innerHTML = "Play all";
                }
            }

            // open all signals
            let signals = document.querySelectorAll(".more-text-toggle, .more-text-content");
            for (var i = 0; i < signals.length; i++) {
                let signal = signals[i];
                signal.classList.add("active");
            }
        }

        if (target.closest(".audio-resetall")) {
            let players = document.getElementsByTagName("audio");
            for (var i = 0; i < players.length; i++) {
                let player = players[i];
                player.pause();
                player.currentTime = 0;

                let playerBtn = player.closest(".audio-container").querySelector(".audio-playpause");
                playerBtn.classList.replace("icon-pause", "icon-play");
                playerBtn.innerHTML = "Play";
            }
        }

        if (target.closest(".audio-jumpall")) {
            let players = document.getElementsByTagName("audio");
            let value = document.getElementById("jumpall-pos").value;
            for (var i = 0; i < players.length; i++) {
                let player = players[i];
                player.currentTime = value;
            }
        }

        // play and pause audio player
        if (target.closest(".audio-playpause")) {
            let button = target.closest(".audio-playpause");
            let player = button.closest(".audio-container").querySelector("audio");
            if (player.paused || player.ended) {
                player.play();
                button.classList.replace("icon-play", "icon-pause");
                button.innerHTML = "Pause";
            } else {
                player.pause();
                button.classList.replace("icon-pause", "icon-play");
                button.innerHTML = "Play";
            }
        }

        // stop audio player
        if (target.closest(".audio-stop")) {
            let button = target.closest(".audio-stop");
            let container = button.closest(".audio-container");
            let player = container.querySelector("audio");
            let progress = container.querySelector("progress");

            // reset playpause button
            let playpause = container.querySelector(".audio-playpause");
            playpause.classList.replace("icon-pause", "icon-play");
            playpause.innerHTML = "Play";

            player.pause();
            player.currentTime = 0;
            progress.value = 0;
        }

        // jump to position in progress bar
        if (target.closest(".audio-progress")) {
            let elem = target.closest(".audio-progress");
            let container = elem.closest(".audio-container");
            let player = container.querySelector("audio");
            var rect = elem.getBoundingClientRect();    // to get absolute position
            var pos = (event.pageX - rect.left) / elem.offsetWidth;
            player.currentTime = pos * player.duration;
        }

        // navigate to page
        if (target.closest(".link--page")){
            event.preventDefault();
            let link = target.closest(".link--page");

            // change language if requested
            if (link.hasAttribute("data-set-lang"))
                sessionStorage.setItem("lang", link.getAttribute("data-set-lang"));

            // open in new window if meta key was pressed
            if (event.metaKey){
                let url = window.location.protocol+"//"+
                            window.location.hostname+":"+
                            window.location.port+"/"+
                            sessionStorage.getItem("lang")+
                            link.getAttribute("href");
                window.open(url);

            // translate url into state object
            } else {
                let link = target.closest(".link--page");
                let url = link.getAttribute("href");
                let hash = url.split('#')[1];
                let stateObj = {};
                let parts;

                if (hash == undefined)
                    parts = url.split('?');
                else {
                    parts = url.split('#')[0].split('?');
                    stateObj["article"] = {};
                    stateObj["article"]["hash"] = "#"+hash;
                }

                stateObj["route"] = parts[0];

                // if article parameters are present
                if (parts[1] != undefined) {
                    let paramsString = parts[1].split('&');
                    if (stateObj["article"] == undefined)
                        stateObj["article"] = {};

                    for (var i = 0; i < paramsString.length; i++) {
                        var temp = paramsString[i].split("=");
                        stateObj["article"][temp[0]] = temp[1];
                    }
                }

                buildPage(stateObj);
            }
        }

        // enter fullscreen
        if (target.closest(".icon-fullscreen")){
            document.getElementById('app').classList.add('hide-header');
            document.querySelector(".icon-fullscreen").classList.replace("icon-fullscreen", "icon-fullscreen_exit");
        } // exit fullscreen
        else if (target.closest(".icon-fullscreen_exit")){
            document.getElementById('app').classList.remove('hide-header');
            document.querySelector(".icon-fullscreen_exit").classList.replace("icon-fullscreen_exit", "icon-fullscreen");
        }

        // click on minimap
        if (target.closest("#minimap")){

            let el = document.getElementById("minimap");
            let rect = el.getBoundingClientRect();
            let viewport = document.getElementById("minimap__viewport");
            let pct = map(event.pageY-(viewport.offsetHeight/2), rect.top, rect.top+rect.height, 0, 100);
            let outline = document.querySelector(".list--outline");
            let targetPx = map(pct, 0, 100, 0, outline.offsetHeight);
            document.getElementById("app__body").scrollTop = targetPx;
            drawMinimap();
        }

        // previous page
        if (target.closest(".page-prev:not(.disabled)"))
            flipPage("prev");

        // next page
        if (target.closest(".page-next:not(.disabled)"))
            flipPage("next");

        // toggle menu overlay
        if (target.closest(".menu-toggle"))
            toggleOverlay("menu");

        // toggle search overlay
        if (target.closest(".search-toggle"))
            toggleOverlay("search");

        // close overlay
        if (target.closest(".media-close"))
            hideOverlay();

        // more list toggler
        if (target.closest(".more-toggler")){
            let list = target.closest(".more-toggler").nextElementSibling;

            if (list.classList.contains("active")) {
                list.classList.remove("active");

            } else {

                let allLists = document.querySelectorAll('.list--more');
                allLists.forEach(function(item){
                    item.classList.remove("active");
                });

                list.classList.add("active");
            }
        } else {
            let allLists = document.querySelectorAll('.list--more');
            allLists.forEach(function(item){
                item.classList.remove("active");
            });
        }

        // hide more list
        if (target.closest('.list--more')){
            if (target.closest('.list--more').classList.contains("active")){
                target.closest('.list--more').classList.remove("active");
            }
        }

        // hide overlay on click in empty space
        if (!document.getElementById('overlay').classList.contains("hidden")){
            if (!target.closest('#overlay > *') && !target.closest('#app__header')){
                hideOverlay();
            }
        }

        // link--media click
        if (target.closest(".link--media")){
            var path = target.closest(".link--media").getAttribute("href");
            var caption = target.closest(".link--media").getAttribute("title");
            let type = getMediaType(path);

            // handle media
            if (["footnote", "image", "video", "audio", "pdf"].includes(type)){
                event.preventDefault();
                if (target.closest(".link--media").classList.contains('collection')){
                    let collection = target.closest(".link--media").getAttribute('data-collection');
                    displayMedia(path, caption, collection);
                } else {
                    displayMedia(path, caption);
                }
            }
        }
        
        // mark current selector in cichlid soundscape
        if (target.closest('[id^="selector__"] .list--more a')){
            event.preventDefault();

            let list = target.closest('[id^="selector__"] .list--more');
            let item = target.closest('[id^="selector__"] .list--more li');

            let allItems = list.querySelectorAll('li');
            allItems.forEach(function(item) {
                item.setAttribute("data-state", "");
            });

            item.setAttribute("data-state", "active");
        }
    }, {capture: false});

    // register keydown events
    window.addEventListener("keydown", function(event) {

        var target = event.target;

        if (event.defaultPrevented) {
            return; // Do nothing if the event was already processed
        }

        switch (event.key) {
            case "ArrowLeft":
                if (document.getElementById("article__text")) flipPage("prev");
                break;
            case "ArrowRight":
                if (document.getElementById("article__text")) flipPage("next");
                break;
            case "ArrowUp":
                if (document.getElementById("article__text")) jumpToPct(0);
                break;
            case "ArrowDown":
                if (document.getElementById("article__text")) jumpToPct(100);
                break;
            case "Enter":
                if (target.id == 'article__position') jumpToPage(target.value);
                else if (target.id == 'input--search') searchPage(target.value);
                break;
            default:
                return;
        }

      // Cancel the default action to avoid it being handled twice
      event.preventDefault();
    }, {capture: false});

    // mousewheel events
    document.getElementById("app").addEventListener('wheel', function(event){
        var target = event.target;

        // handle scrollwheel on minimap
        if (target.closest("#minimap")) {
            event.preventDefault();
            let container = document.getElementById("app__body");
            container.scrollTop += event.deltaY;
        }

        // update minimap viewport
        if (document.querySelector('.list--outline')) {
            drawMinimap();
        }

        // prevent mouse scrolling in article view
        if (target.closest("#article__text")){
            event.preventDefault();
            var threshold = 10;

            // toggle header
            if (Math.abs(event.deltaY) > threshold){
                if (event.deltaY < 0) {
                    flipPage('next');
                }
                if (event.deltaY > 0) {
                    flipPage('prev');
                }
            }

            // flip page
            if (Math.abs(event.deltaX) > 40){
                if (event.deltaX < 0) {
                    flipPage('prev');
                }
                if (event.deltaX > 0) {
                    flipPage('next');
                }
            }
        }
    }, {capture: false});

    // media link hover effect
    document.getElementById("app").addEventListener("mousemove", function(event){
        var target = event.target;

        // dragging minimap vieweport
        if (minimapDragging){
            let el = document.getElementById("minimap");
            let rect = el.getBoundingClientRect();
            let viewport = document.getElementById("minimap__viewport");
            let pct = map(event.pageY-(viewport.offsetHeight/2), rect.top, rect.top+rect.height, 0, 100);
            let outline = document.querySelector(".list--outline");
            let targetPx = map(pct, 0, 100, 0, outline.offsetHeight);
            document.getElementById("app__body").scrollTop = targetPx;
            drawMinimap();
        }

        // position link preview
        if (document.querySelector(".link--preview")) {

            if (target.closest(".link--media")) {
                var elem = target.closest(".link--media");

                //set transform position
                if (document.querySelector(".link--preview")){
                    var preview = document.querySelector(".link--preview");
                    var offset = 5;
                    preview.style.top = event.pageY - offset +"px";
                    preview.style.left = event.pageX + offset +"px";
                }
            }
        }
    }, {capture: false, passive: true});

    // mouseover event
    document.getElementById("app").addEventListener("mouseover", function(event){

        var target = event.target;

        // -------------
        // add target links to outline item
        if (document.querySelector(".list--outline")){

            if (target.closest(".outline-item") && !document.querySelector(".targets")){

                var href = target.closest("a").getAttribute("href");
                var type = getMediaType(href);

                var links = document.createElement('ul');
                links.classList.add('targets');
                var url = target.closest(".outline-item").querySelector('a').href;

                links.innerHTML = `<li><a href="`+currentPage.route+`?view=read#`+
                target.closest(".outline-item a").id+
                `" class="link--page icon-subdirectory_arrow_right">`+stringList["show-in-article"][sessionStorage.getItem("lang")]+`</a></li>`;
                target.closest(".outline-item").appendChild(links);

                // remove target links on mouseleave
                target.closest(".outline-item").addEventListener("mouseleave", function(event){
                    var target = event.target;

                    if (target.closest(".outline-item") && document.querySelector(".targets")){
                        var links = document.querySelectorAll(".targets");

                        // remove all preview items
                        links.forEach(function(link) {
                            link.remove();
                        });
                    }
                }, {capture: false, passive: true});
            }
        }

        // -------------
        // create hover preview in text view
        if (!document.querySelector(".list--outline") && 
            !document.querySelector(".media-viewer__nav")) {

            if (target.closest(".link--media")) {

                // get href attribute
                var href = target.closest("a").getAttribute("href");
                var type = getMediaType(href);

                if (type != "footnote") {

                    var preview = document.createElement("div");
                    preview.classList.add("link--preview", "visible");
                    preview.style.transform = "translate( 0, -100% )";
                    var offset = 5;
                    preview.style.top = event.pageY - offset +"px";
                    preview.style.left = event.pageX + offset +"px";

                    var title = document.createTextNode("Link");
                    if (target.closest("a").hasAttribute("title"))
                        title = document.createTextNode(target.closest("a").getAttribute("title"));

                    //check type of link and insert according html
                    if (target.classList.contains("collection")) {  // is a collection of links
                        let count = article.collections[target.getAttribute("data-collection")].length;
                        preview.classList.add("icon-collections");
                        preview.innerHTML = stringList["media-collection"][sessionStorage.getItem("lang")]+" ("+count+")";

                        // this requires collections to have a png thumbnail with the same name as the first file
                        let firstFilePath = article.collections[target.getAttribute("data-collection")][0].path;
                        let firstFilename = firstFilePath.replace(/^.*[\\\/]/, '');
                        let name = firstFilename.replace(/\.[^/.]+$/, "")
                        let posterFilePath = mediaPath+name+".png";

                        let imageNode = document.createElement("img");
                        imageNode.src = posterFilePath;
                        preview.appendChild(imageNode);

                    } else if (type == "image") { // is image
                        var imageNode = document.createElement("img");
                        imageNode.src = href;
                        preview.appendChild(imageNode);

                    } else if (type == "pdf") { // is pdf
                        // this requires pdfs to have a png thumbnail with the same name
                        var imageNode = document.createElement('img');
                        imageNode.src = href.substr(0, href.lastIndexOf(".")) + ".png";
                        preview.appendChild(imageNode);

                    } else if (type == "website") { // is website
                        preview.classList.add('icon-link');
                        preview.appendChild(title);
                    
                    } else if (type == "video") { // is website
                        // this requires videos to have a png thumbnail with the same name
                        var imageNode = document.createElement('img');
                        imageNode.src = href.substr(0, href.lastIndexOf(".")) + ".png";
                        preview.appendChild(imageNode);
                        preview.classList.add("video");

                    } else if (type == "internal") {
                        preview.classList.add('icon-link');
                        preview.appendChild(title);
                    }

                    // appending to app element leads to jump to top of page in safari... (reason unknonwn)
                    // document.getElementById("app").appendChild(preview);
                    document.querySelector("body").appendChild(preview);
                }
            }
        }
    }, {capture: false, passive: true});

    // mouseout event
    document.getElementById("app").addEventListener("mouseout", function(event){
        var target = event.target;

        if (target.closest(".link--media") && document.querySelector(".link--preview")){
            var previews = document.querySelectorAll(".link--preview");

            // remove all preview items
            previews.forEach(function(preview) {
                preview.classList.remove("visible");
                preview.addEventListener("transitionend", function(e){
                    preview.remove();
                }, false);
            });
        }
    }, {capture: false, passive: true});

    // save position of touch start for direction recognition
    var startY;
    var startX;

    // uncommented because this disables click event listeners.
    document.getElementById("app").addEventListener("touchstart", function(event){
        var target = event.target;
        if (target.closest("#article__text")) {
            var touches = event.changedTouches;
            startY = touches[0].pageY;
            startX = touches[0].pageX;
        }
    }, {capture: false});

    // touchmove interactions for article page
    var flipPageDir;
    document.getElementById("app").addEventListener("touchmove", function(event){
        var target = event.target;

        if (target.closest("#article__text")) {
            
            var touches = event.changedTouches;
            var currentY = touches[0].pageY;
            var currentX = touches[0].pageX;
            var distY = Math.abs(startY-currentY);
            var distX = Math.abs(startX-currentX);
            var threshold = 40;

            if (distX > distY)  // only prevent default for X scroll
                event.preventDefault();

            // register page flip on horizontal swiping
            if (distX > threshold)
                if (currentX > startX)
                    flipPageDir = 'prev';
                else
                    flipPageDir = 'next';
        }
    }, {capture: false});

    // flip page on horizontal swiping
    document.getElementById("app").addEventListener("touchend", function(event){
        var target = event.target;
        if (flipPageDir)
            flipPage(flipPageDir);
        flipPageDir = '';
    }, {capture: false});

    // handle minimap dragging
    document.getElementById("app").addEventListener("mousedown", function(event){
        var target = event.target;

        if (target.closest("#minimap__viewport")){
            minimapDragging = true;
            document.getElementById("minimap__viewport").setAttribute("data-state", "grabbed");
        }
    }, {capture: false});

    document.getElementById("app").addEventListener("mouseup", function(event){
        var target = event.target;

        // function to get character of currently selected text
        if (target.closest("#article__text") && // click within article
            document.getElementById("article__text") && // article exists
            !target.closest(".link--media")) {  // not a media link klicks
            if (document.getElementById("selection-start"))
                document.getElementById("selection-start").remove();
            if (document.getElementById("selection-start__btn"))
                document.getElementById("selection-start__btn").remove();
            if (document.getElementById("mark"))
                document.getElementById("mark").remove();
            var selObj = window.getSelection();
            var selRange = selObj.getRangeAt(0);
            let selectStart = document.createElement("span");
            selectStart.id = "selection-start";
            selRange.insertNode(selectStart);

            let selBtn = document.createElement("div");
            selBtn.id = "selection-start__btn";
            selBtn.innerHTML = stringList["cite"][sessionStorage.getItem("lang")]
            document.getElementById("page").appendChild(selBtn);
            let rect = selectStart.getBoundingClientRect();
            selBtn.style.left = Math.round(rect.left)+"px";
            selBtn.style.top = Math.round(rect.top)+"px";
        }

        if (minimapDragging){
            minimapDragging = false;
            document.getElementById("minimap__viewport").setAttribute("data-state", "");
        }
    }, {capture: false});

    // register resize event
    window.addEventListener("resize", resizeThrottler, {capture: false, passive: true});
    var resizeTimeout;

    function resizeThrottler() {
        // ignore resize events as long as an actualResizeHandler execution is in the queue
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function() {
            actualResizeHandler();
            // The actualResizeHandler will execute at a rate of 15fps
        }, 20);
    }

    function actualResizeHandler() {
        // handle the resize event

        // -------------
        // read view
        if (document.getElementById('article__text')) {
            setTimeout(prepareArticlePage());
        }

        // -------------
        // outline view
        if (document.querySelector('.list--outline')) {
            repositionOutlineItems();
            drawOutline();
            drawMinimap();
        }
    }
}


// ==============================
// navigate to project when history is changed
window.onpopstate = function(event) {

    // determine language from URL
    let urlParts = window.location.pathname.split("/");
    if (["de", "en"].includes(urlParts[1]))
        sessionStorage.setItem('lang', urlParts[1]);

    // build requested page if state object is provided
    if (event.state)
        buildPage(event.state, false);

    // if texas soundscape page build interface
    else if (window.location.hash !== ps.toString() && document.getElementById('tx_player')) {
        ps.parseHash(window.location.hash);
        updateInterface();
        updateMedia();

    // fall back to home page
    } else {
        buildPage({route: 'home'}, false);
    }
};


// ==============================
// search page
function searchPage(term){
    const matches = searchIndex.search(term, {
        fields: {
            title: {boost: 2},
            body: {boost: 1}
        }
    });

    let container = document.getElementById("search-results");
    container.innerHTML = "";

    if (matches.length != 0) {
        let count = 0;
        for (let match of matches) {
            if (match.doc.lang == sessionStorage.getItem("lang")) { // only show results for current language
                count++;

                let str = match.doc.body;
                let pos = str.search(new RegExp(term, "i"));
                let substring = str.substr(pos, 100);

                let result = document.createElement("li");
                result.classList.add("search-result", match.doc.type);

                let excerpt = document.createElement("a");
                excerpt.classList.add("link--page", "search__excerpt");

                if (match.doc.route.includes("?view=read")) // add character position if result in read view
                    excerpt.href = match.doc.route+"&char="+pos
                else 
                    excerpt.href = match.doc.route;

                excerpt.innerHTML = "..."+substring+"...";
                result.appendChild(excerpt);

                let info = document.createElement("div");
                info.classList.add("search__info");
                result.appendChild(info);

                if (match.doc.type == "article") {
                    let parent = document.createElement("a");
                    parent.classList.add("link--page", "search__parent");
                    parent.innerHTML = stringList["rigi-talk"][sessionStorage.getItem("lang")];
                    parent.href = "rigi";
                    info.appendChild(parent);
                }

                let page = document.createElement("a");
                page.classList.add("link--page", "search__page");
                page.innerHTML = match.doc.title;
                page.href = match.doc.route;
                info.appendChild(page);

                // count occurences of string
                let regex = new RegExp(term, "gi");
                let occurences = str.match(regex);
                if (occurences) {
                    let occurenceCount = document.createElement("div");
                    occurenceCount.classList.add("search__matches");
                    occurenceCount.innerHTML = "("+occurences.length+" "+stringList["matches"][sessionStorage.getItem("lang")]+")";
                    info.appendChild(occurenceCount);
                }

                container.appendChild(result);
            }
        }

        // no search result in current language
        if (count == 0) {
            let message = document.createElement("p");
            message.innerHTML = stringList["no-search-results"][sessionStorage.getItem("lang")];
            container.appendChild(message);
        }

    // no search result at all
    } else {
            let message = document.createElement("p");
            message.innerHTML = stringList["no-search-results"][sessionStorage.getItem("lang")];
            container.appendChild(message);
    }
}


// ==============================
// helper functions

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getMediaType(path){
    let type;

    if (path.startsWith("http"))
        type = "website";
    else if (path.endsWith(".png") || path.endsWith(".jpg") || path.endsWith(".gif"))
        type = "image";
    else if (path.endsWith(".pdf"))
        type = "pdf";
    else if (path.endsWith(".wav") || path.endsWith(".mp3"))
        type = "audio";
    else if (path.endsWith(".mp4"))
        type = "video";
    else if(path.startsWith("^"))
        type = "footnote";
    else
        type = "internal";

    return type;
}

function map (num, in_min, in_max, out_min, out_max) {
    // taken from
    // (https://stackoverflow.com/questions/10756313/javascript-jquery-map-a-range-of-numbers-to-another-range-of-numbers/23202637)
    return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

const loadFile = url =>
    fetch(url)
        .then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        }).then(function(response) {
            return response.text();
        }).catch(function(error) {
            console.log(error);
        });

const checkImage = path =>
    // taken from https://codereview.stackexchange.com/questions/128587/check-if-images-are-loaded-es6-promises
    new Promise(resolve => {
        const img = new Image();
        img.onload = () => resolve({path, status: 'ok'});
        img.onerror = () => resolve({path, status: 'error'});

        img.src = path;
    });

var playPause = null; // play/pause control element

function initDatenlauschenDocumentation() {
    // prepare page
    var page = document.getElementById('page');
    page.innerHTML = '';
    var pageBody = document.createElement('div');
    pageBody.id = 'page__body';

    // title
    var title = document.createElement('h1');
    if (sessionStorage.getItem("lang") == "de") {
        title.innerHTML = "Datenflüssen lauschen. Wie die Umwelt in den Computer kommt. Deutsches Schifffahrtsmuseum, Bremerhaven 2022 (mit Valentina Vuksic, Birk Weiberg, Christoph Stähli) (EA)";
    } else {
        title.innerHTML = "Listening to Data. How the environment gets into the computer. German Maritime Museum, Bremerhaven 2022 (with Valentina Vuksic, Birk Weiberg, Christoph Stähli) (SE)";
    }
    pageBody.appendChild(title);

    // video
    var player = document.createElement('div');
    player.setAttribute('id', 'player');

    pageBody.appendChild(player);

    var sources = null;
    if (sessionStorage.getItem("lang") == "de") {
      var sources = {
          'video/webm': '/media/tx/documentation/video/DSM_1920x1080_2023-10-16_HD_DE_WEB_2023-10-19.webm',
      };
    } else {
      var sources = {
          'video/webm': '/media/tx/documentation/video/DSM_1920x1080_2023-10-16_HD_EN_WEB_2023-10-19.webm',
      };
    }

    var imageSrc = '';
    if (sessionStorage.getItem("lang") == "de") {
      var imageSrc = '/media/tx/documentation/video/DSM_1920x1080_2023-10-16_HD_DE_WEB_2023-10-19.jpg';
    } else {
      var imageSrc = '/media/tx/documentation/video/DSM_1920x1080_2023-10-16_HD_EN_WEB_2023-10-19.jpg';
    }

    loadVideo(sources, imageSrc, player);

    // leaflet
    var leaflet = document.createElement('h1');
    var leaflet_pdf = document.createElement('a');
    leaflet_pdf.setAttribute('class', 'link--media');
    if (sessionStorage.getItem("lang") == "de") {
        leaflet_pdf.setAttribute('href', '/media/Saaltext_DSM-2022.pdf');
        leaflet_pdf.innerHTML = "Saaltext";
    } else {
        leaflet_pdf.setAttribute('href', '/media/Saaltext_DSM-2022.pdf');
        leaflet_pdf.innerHTML = "Exhibition text";
    }
    leaflet.appendChild(leaflet_pdf);
    pageBody.appendChild(leaflet);


    // PDF Dokumentation
    var doc = document.createElement('h1');
    var doc_pdf = document.createElement('a');
    doc_pdf.setAttribute('class', 'link--media');
    if (sessionStorage.getItem("lang") == "de") {
        doc_pdf.setAttribute('href', '/media/20231011-Daten-lausche_Dokumentation-DSM-Bremerhaven2022_de-lr2.pdf');
        doc_pdf.innerHTML = "PDF";
    } else {
        doc_pdf.setAttribute('href', '/media/20231011-Daten-lausche_Dokumentation-DSM-Bremerhaven2022_de-lr2.pdf');
        doc_pdf.innerHTML = "PDF";
    }
    doc.appendChild(doc_pdf);
    pageBody.appendChild(doc);

    page.appendChild(pageBody);

}

function setPlayPause(play) {
    if (playPause != undefined) {
        if (play) {
            playPause.classList.remove('icon-play');
            playPause.classList.add('icon-pause');
        } else {
            playPause.classList.remove('icon-pause');
            playPause.classList.add('icon-play');
        }
    }

}

function loadVideo(sources, imageSrc, el) {

    var video = document.createElement('video');

    Object.keys(sources).forEach(function (key) {
        var source = document.createElement('source');
        source.type = key;
        source.src = sources[key];

        video.appendChild(source);
    });

    // video.setAttribute('controls', '');
    video.setAttribute('width', '100%');
    video.setAttribute('poster', imageSrc);

    video.addEventListener('ended', function() {
        setPlayPause(false);
    }, true);

    video.addEventListener('pause', function() {
        setPlayPause(false);
    }, true);

    video.addEventListener('play', function() {
        setPlayPause(true);
    }, true);


    // define controls

    playPause = document.createElement('a');
    playPause.setAttribute('class', 'link icon-play click-item');
    playPause.setAttribute('title', 'Play/Pause');

    function togglePlay() {
        if (video.paused) {
            video.play();
            // setPlayPause(true);
        } else {
            video.pause();
            // setPlayPause(false);
        }
    }

    playPause.addEventListener('click', togglePlay);
    video.addEventListener('click', togglePlay);

    var fullscreen = document.createElement('a');
    fullscreen.setAttribute('class', 'link icon-fullscreen click-item');
    fullscreen.setAttribute('title', 'Fullscreen');

    function toggleFullscreen() {
        if (video.requestFullScreen) {
            video.requestFullScreen();
        } else if (video.webkitRequestFullScreen) {
            video.webkitRequestFullScreen();
        } else if (video.mozRequestFullScreen) {
            video.mozRequestFullScreen();
        }
    }

    fullscreen.addEventListener('click', toggleFullscreen);

    // add controls
    var controls = document.createElement('div');
    controls.setAttribute('id', 'timecontrol');
    controls.appendChild(playPause);
    controls.appendChild(fullscreen);

    // append video and controls
    el.appendChild(video);
    el.appendChild(controls);


}

const searchEngine = elasticlunr(function () {   // create search index
    this.addField('type');  // html, article, script
    this.addField('lang');
    this.addField('route');
    this.addField('title');
    this.addField('body');
    this.setRef('id');
    this.saveDocument(true);
});

// read pages file
fetch("/pages.json")
    .then(function(response) { return response.json(); })
    .then(function(data) {

    //save pages to global variable
    let pages = data.pages;
    let promises = [];
    let langPrefixes = ["en", "de"];
    let pageTypes = [
        {
            name : "static",
            idString: ".html",
            path: "/components/contents/"
        },{
            name : "article",
            idString: ".md",
            path: "/components/articles/"
        },{
            name : "script",
            idString: "()",
            path: "scripts/"
        }
    ];

    // load all content files
    let id = 0;
    for (let page of pages) {
        for (let pageType of pageTypes) // iterate over page types
            if (page.content.endsWith(pageType.idString))   // find matching page type
                if (pageType.name != "script")  // ignore all script pages
                    for (let lang of langPrefixes) {  // iterate over languages
                        let path = pageType.path+lang+"-"+page.content;
                        let promise  = fetch(path)
                            .then(function(response) { return response.text(); });
                        promises.push(promise);

                        promise.then(function(data) {

                            let doc = {
                                id: id,
                                type: pageType.name,
                                lang: lang,
                                route: page.route,
                                title: page.title[lang],
                                body: data
                            };

                            if (doc.type == "article") {
                                doc.body = doc.body.split('[^1]: ')[0]; // remove footnotes
                                doc.body = doc.body.replace(/\((.+:)\)/g, '$1'); // remove speakers name
                                doc.body = doc.body.replace(/\[\^([0-9]*)\]/g, '<a href="^$1"><sup>[$1]</sup></a>')
                                let parts = doc.body.split("---\n---\n\n");

                                for (const [index, part] of parts.entries()) {  // split abstract and text view into two search items
                                    doc.body = converter.makeHtml(part);   // convert to html
                                    doc.body = stripHTML(doc.body);
                                    if (index == 1)  // add search parameters to route of read view
                                        doc.route = doc.route+"?view=read";

                                    doc.id = id;
                                    searchEngine.addDoc(doc);
                                    id++;
                                }
                                
                            } else if (doc.type == "static") {
                                doc.body = stripHTML(doc.body);
                                searchEngine.addDoc(doc);
                                id++;
                            }
                        });
                    }
    }

    function stripHTML(html) {
        let pureText;
        var temporalDivElement = document.createElement("div");
        temporalDivElement.innerHTML = html;
        pureText = temporalDivElement.textContent || temporalDivElement.innerText || "";
        pureText = pureText.replace(/\s\s+/g, ' ');
        return pureText;
    }

    // print search index to console
    Promise.all(promises)
    .then(function(response) {
        console.log(JSON.stringify(searchEngine));
    });
});
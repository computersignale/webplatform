// contains information about all locations
var content;
// location data
var locations = {};
var timeSelectors = [];
// location thumbs
var thumbs = [];
// video player
var video;
var info;
var controls;
// audio players
var audios = [];
var mics = {'a': 'acoustic', 'c': 'contact', 'e': 'electromagnetic'};
var vis = {'s': 'spectrogram', 'w': 'waveform'};

function initTX() {
    console.log('initTX ' + window.location.hash);

    // prepare page
    var page = document.getElementById('page');
    page.innerHTML = '';
    var pageBody = document.createElement('div');
    pageBody.id = 'page__body';
    var title = document.createElement('h1');
    title.innerHTML = "Cichlid #3, Soundscape Texas (Aug. 21, 2014)";
    pageBody.appendChild(title);
    page.appendChild(pageBody);

    loadMetaData();
    // buildInterface();
}

function createElement(element, attributes, parent) {
    if (typeof(element) === "undefined") {
        return false;
    }
    var el = document.createElement(element);
    if (typeof(attributes) === 'object') {
        for (var key in attributes) {
            if (key == 'innerText' || key == 'innerHTML') {
                el[key] = attributes[key];
            } else {
                el.setAttribute(key, attributes[key]);
            }
        }
    }
    if (parent) {
        parent.appendChild(el);
    }
    return el;
}

function buildInterface() {
    // prepare the interface once independant from the current player state
    var i, j;
    var c = createElement('div', {'id': 'tx-player'}, document.getElementById('page__body'));
    var s = createElement('div', {'id': 'selector'}, c);
    var sh = createElement('ul', {'id': 'selector__hour'}, s);
    var shl = createElement('li', {'innerHTML': '<label class="selector__label">time</label><button class="more-toggler">00:00</button>'}, sh);
    var shlu = createElement('ul', {'class': 'list--more'}, shl);
    for (i=0; i<24; i++) {
        timeSelectors.push(new TimeSelector(i, shlu));
    }
    var sl = createElement('ul', {'id': 'selector__location'}, s);
    var sll = createElement('li', {'innerHTML': '<label class="selector__label">location</label><button class="button more-toggler">Fish facilities</button>'}, sl);
    var sllu = createElement('ul', {'class': 'list--more'}, sll);
    for (var l in locations) {
        thumbs.push(new LocationThumb(locations[l], sllu));
    }
    var vc = createElement('div', {'id': 'video-current'}, c);
    video = createElement('video', {'id': 'videoPlayer', 'width': '100%'}, vc);
    video.addEventListener('ended', function () {
        onVideoEnded();
    }, true);
    video.addEventListener('pause', function () {
        onVideoPause();
    }, true);
    video.addEventListener('play', function () {
        onVideoPlay();
    }, true);
    video.addEventListener('emptied', function () {
        onVideoPause();
    }, true);
    video.addEventListener('seeked', function () {
        if (ps.getAutoPlay()) {
            this.play();
            ps.clearTargetTime();
            ps.clearAutoPlay();
        } else {
            this.pause();
        }
    }, true);
    video.addEventListener('canplay', function () {
        if (ps.getTargetTime() !== null) {
            gotoSec(ps.getTargetTime());
            ps.clearTargetTime();
        } else if (ps.getAutoPlay()) {
            ps.clearAutoPlay();
            this.play();
        }
    }, true);
    video.addEventListener('timeupdate', function () {
        onTimeUpdate(this.currentTime);
    }, true);
    info = new Info(vc);
    controls = new Controls(c);
    var a = createElement('div', {'id': 'audio'}, c);
    audios = [];
    for (i=0; i<3; i++) {
        audios[i] = new AudioPlayer(i);
        a.appendChild(audios[i].getElement());
    }
    window.addEventListener('keyup', function (e) {
        switch (e.which) {
            case 32:
                // space
                togglePlay();
                break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });
    window.addEventListener('keydown', function (e) {
        var hash, a, time, seg, vidpos, min, sec;
        switch (e.which) {
            case 37: // left
                if (e.shiftKey) {
                    goPrevHour(false);
                } else {
                    goPrevSec();
                }
                break;
            case 39: // right
                if (e.shiftKey) {
                    goNextHour(false);
                } else {
                    goNextSec();
                }
                break;
            case 76: // l for copy link
                hash = ps.toString();
                // /13:00/s/1,0,0/
                a = hash.split('/');
                time = a[1];
                seg = ps.getSegment();
                // TODO: get current time
                vidpos = Math.round(video.node.currentTime);
                min = seg * 10 + Math.floor(vidpos / 60);
                sec = vidpos % 60;
                time = time.substring(0, 3) + ('0' + min).slice(-2) + ':' + ('0' + sec).slice(-2);
                a[1] = time;
                hash = a.join('/');
                alert(window.location.origin + window.location.pathname + hash);
                break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });
}

function loadMetaData() {
    fetch('/components/contents/content-tx.json')
    .then(function(response) {
        return response.json();
    })
    .then(function(content) {
        var c, l, k;
        for (c in content.locations) {
            l = {};
            for (k in content.locations[c]) {
                if (content.locations[c][k].toString() !== '') {
                    l[k] = content.locations[c][k];
                }
            }
            locations[content.locations[c].identifier] = l;
        }
        ps.parseHash(window.location.hash);
        buildInterface();
        updateInterface();
        updateMedia();
    });
}

function TimeSelector(h, c) {
    this._hour = ('0' + h).slice(-2);
    // TODO: There is no href for the anchor here - change to button?
    this._node = createElement('a', {'href': '', 'innerText': this._hour}, createElement('li', null, c));
    this._node.addEventListener('click', function () {
        gotoHour(h);
    }, true);
    this.getNode = function() {
        return this._node;
    }
    this.getHour = function() {
        return this._hour;
    }
    this.setSelected = function(selected) {
        if (selected) {
            this._node.setAttribute('data-state', 'active');
        } else {
            this._node.removeAttribute('data-state');
        }
    }
}

function LocationThumb(data, c) {
    var t = this;
    this._data = data;
    this._node = createElement('li', null, c);
    // TODO: There is no href for the anchor here - change to button?
    var a = createElement('a', {'href': ''}, this._node);
    a.addEventListener('click', function () {
        showLocation(t._data.identifier);
    }, true);
    this._label = createElement('label', {'innerHTML': this._data.label}, a);
    this._img = createElement('img', null, a);
    this.getNode = function() {
        return this._node;
    }
    this.getIdentifier = function() {
        return this._data.identifier;
    }
    this.setTime = function(hours, minutes) {
        this._img.src = '/media/tx/' + this._data.media + '/videothumbs/' + hours + '-' + minutes + '.jpg';
    }
    this.setSelected = function(selected) {
        if (selected) {
            this._node.setAttribute('data-state', 'active');
        } else {
            this._node.removeAttribute('data-state');
        }
    }
}

function Info(c) {
    this._node = createElement('div', {'id': 'location'}, c);
    this._label = createElement('label', null, createElement('div', {'id': 'location__name', 'class': 'icon-location'}, this._node));
    this._map_link = createElement('a', {'id': 'location__map', 'href': '', 'target': 'blank', 'class': 'icon-map link click-item', 'innerHTML': '<label>map</label>'}, this._node);
    this.setLabel = function(label) {
        this._label.innerHTML = label;
    }
    this.setMapLink = function(lat_, long_) {
        this._map_link.href = 'http://maps.google.com/maps?q='+ lat_ + ',' + long_;
        this._map_link.style.display = (lat_ && long_)? 'inline' : 'none';
   }
}

function Controls(c) {
    this._node = createElement('div', {'id': 'timecontrol'}, c);
    var prevHour = createElement('a', {'class': 'link icon-rewind-1hour click-item', 'title': 'Rewind 1 Hour'}, this._node);
    prevHour.addEventListener('click', function () {
        goPrevHour();
    }, true);
    var prevSec = createElement('a', {'class': 'link icon-rewind-1sec click-item', 'title': 'Rewind 1 Second'}, this._node);
    prevSec.addEventListener('click', function () {
        goPrevSec();
    }, true);
    this._playPause = createElement('a', {'class': 'link icon-play click-item', 'title': 'Play/Pause'}, this._node);
    this._playPause.addEventListener('click', function () {
        togglePlay();
    }, true);
    var nextSec = createElement('a', {'class': 'link icon-forward-1sec click-item', 'title': 'Forward 1 Second'}, this._node);
    nextSec.addEventListener('click', function () {
        goNextSec();
    }, true);
    var nextHour = createElement('a', {'class': 'link icon-forward-1hour click-item', 'title': 'Forward 1 Hour'}, this._node);
    nextHour.addEventListener('click', function () {
        goNextHour();
    }, true);
    this.getNode = function() {
        return this._node;
    }
    this.setPlayPause = function(play_) {
        if (play_) {
            this._playPause.classList.remove('icon-play');
            this._playPause.classList.add('icon-pause');
        } else {
            this._playPause.classList.remove('icon-pause');
            this._playPause.classList.add('icon-play');
        }
    }
    // this.setAudioDisplay = function(display_) {
    //     this._radios['s'].checked = (display_=='s');
    //     this._radios['w'].checked = (display_=='w');
    // }
}

function PlayerState() {
    // default values
    this._location = 'fishfacilities';
    this._dateTime = new Date("Thu Aug 21 2014 12:00:00");
    // time in seconds with the current 10-minute-segment
    this._targetTime = null;
    this.AUDIO_SPEC = 's';
    this.AUDIO_WAVE = 'w';

    this._audioDisplay = this.AUDIO_SPEC;
    this._audioStates = [];

    this._autoPlay = false;
    // this.ignoreHashChange = false;

    this.parseHash = function (hash_) {
        // format: ?loc=chillingstation&time=12:00&display=s&states=0,0,0
        let parsedUrl = new URL(window.location.href);
        let params = {  // default values
            loc:"fishfacilities",
            time:"12:00",
            display:"s",
            states:"1"
        };

        for (let p of parsedUrl.searchParams)   // check parameters
            params[p[0]] = p[1];

        let hashLocation = params.loc;
        let hashTime = params.time;
        let hashAudioDisplay = params.display;
        let hashAudioStates = params.states;
        let hashPlayerState;

        // format: '#fishfacilities/12:00/s/1/'
        // var ha = hash_.substring(1).split('/');
        // console.log('parseHash ' + ha);
        // var hashLocation = ha[0];
        // var hashTime = ha[1];
        // var hashAudioDisplay = ha[2];
        // var hashAudioStates = ha[3];
        // var hashPlayerState = ha[4];

        // read location
        if (locations[hashLocation]) {
            this._location = hashLocation;
        }

        // read time
        if (hashTime) {
            var ta = hashTime.split(':');
            var th = parseInt(ta[0]);
            var tm = parseInt(ta[1]);
            var ts = parseInt(ta[2]);
            if (th >= 0 && th <= 23) {
                this._dateTime.setHours(th);
            }
            if (tm >= 0 && tm <= 59) {
                this._dateTime.setMinutes(tm);
            }
            if (ts >= 0 && ts <= 59) {
                this._dateTime.setSeconds(ts);
            } else {
                this._dateTime.setSeconds(0);
            }
        }
        this._targetTime = (this._dateTime.getMinutes() % 10) * 60 + this._dateTime.getSeconds();
        if (this._targetTime === 0) {
            this._targetTime = null;
        }

        // read audio display state
        this._audioDisplay = (hashAudioDisplay === this.AUDIO_SPEC || hashAudioDisplay === this.AUDIO_WAVE)? hashAudioDisplay : this.AUDIO_SPEC;

        // read audio states
        if (hashAudioStates) {
            var statesArray = hashAudioStates.split(',');
            var i;
            for (i=0; i<statesArray.length; i++) {
                this._audioStates[i] = (statesArray[i] === '1')? 1 : 0;
            }
        }

        // read player state
        this._autoPlay = false;
        if (hashPlayerState === 'play') {
            this.updateUrl(true);
            this._autoPlay = true;
        }
    };

    this.updateUrl = function (replace_=false) {
        // this.ignoreHashChange = true;
        if (replace_) {
            window.history.replaceState(null, '', this.toString());
        } else {
            window.history.pushState(null, '', this.toString());
        }
    };

    this.toString = function () {
        // play state is an argument because its value is not part of this object?
        // var p = (this._autoPlay)? 'play' : '';
        // return '#' + this.getLocation() + '/' + this.getDateTimeString(false) + '/' + this._audioDisplay + '/' + this._audioStates.join(',') + '/' + p;

        let p = (this._autoPlay)? '&play=play' : '';
        return '?loc=' + this.getLocation() + '&time=' + this.getDateTimeString(false) + '&display=' + this._audioDisplay + '&states=' + this._audioStates.join(',') + p;
    };

    this.toShortString = function () {
        // play state is an argument because its value is not part of this object?
        var p = (this._autoPlay)? 'play' : '';
        return '#/' + this.getLocation() + '/' + this.getDateTimeString(false) + '/' + this._audioDisplay + '//' + p;
    };

    this.setLocation = function (location_) {
        console.log('ps.setLocation ' + location_);
        this._location = location_;
        if (locations[this._location].is_real) {
            this.setAudioTracks(locations[this._location].audiorecordings.length);
        } else {
            this.setAudioTracks(0);
        }
        this.setAudioState(0, 1);
        this.updateUrl();
    };

    this.getLocation = function () {
        return this._location;
    };

    this.getDateTimeString = function (seconds_) {
        var d = this.getHours(true) + ':' + this.getMinutes(true, true);
        if (seconds_) {
            d += ':' + ('0' + this._dateTime.getSeconds()).slice(-2);
        }
        return d;
    };

    this.setHours = function (hours_) {
        this._dateTime.setHours(hours_);
        this._dateTime.setMinutes(0);
        this.updateUrl();
    };

    this.getHours = function (leadingZero_) {
        var h = this._dateTime.getHours();
        if (leadingZero_) {
            h = ('0' + h).slice(-2);
        }
        return h;
    };

    this.setMinutes = function (minutes_) {
        this._dateTime.setMinutes(minutes_);
    };

    this.getMinutes = function (leadingZero_, segment_) {
        var m = this._dateTime.getMinutes();
        if (segment_) {
            m = 10 * Math.floor(m / 10);
        }
        if (leadingZero_) {
            m = ('0' + m).slice(-2);
        }
        return m;
    };

    this.getSegment = function () {
        var m = this._dateTime.getMinutes();
        var s = Math.floor(m / 10);
        return s;
    };

    this.setAudioTracks = function (no_) {
        // set the number of existing audio tracks
        this._audioStates = [];
        var i;
        for (i=0; i<no_; i++) {
            this._audioStates[i] = (i === 0)? 1 : 0;
        }
    };

    this.setAudioDisplay = function (type_) {
        // expects PlayerState.AUDIO_SPEC or PlayerState.AUDIO_WAVE
        this._audioDisplay = type_;
    };

    this.getAudioDisplay = function () {
        return this._audioDisplay;
    };

    this.setAudioState = function (id_, active_) {
        // id_ is 0-based, active_ = 1/0
        if (id_<=this._audioStates.length) {
            this._audioStates[id_] = parseInt(active_);
        }
    };

    this.getAudioState = function (id_) {
        if (id_>this._audioStates.length) {
            return false;
        } else {
            return this._audioStates[id_];
        }
    };

    this.setAutoPlay = function (play_) {
        this._autoPlay = play_;
    };

    this.getAutoPlay = function () {
        return this._autoPlay;
    };

    this.clearAutoPlay = function () {
        this._autoPlay = false;
    }

    this.setTargetTime = function (time_) {
        this._targetTime = time_;
    };

    this.getTargetTime = function () {
        return this._targetTime;
    };

    this.clearTargetTime = function () {
        this._targetTime = null;
    };
}

function AudioPlayer(id) {
    var ap = this;
    this._id = id;
    this._element = createElement('div', {'class': 'audio__track', 'id': 'track_' + id});
    var display_container = createElement('div', {'class': 'audio__display'}, this._element);
    display_container.addEventListener('click', function(event_) {
        var p = (event_.pageX-this.getBoundingClientRect().left)/this.getBoundingClientRect().width; 
        onAudioClick(ap, Math.min(Math.max(p,0),1))
    }, true);
    this._display = createElement('img', {'id': 'audioDisplay_' + id}, display_container);
    var segments_container = createElement('div', {'class': 'audio__segments'}, display_container);
    this._segments = [];
    for (var i=0; i<6; i++) {
        var s = createElement('div', {'data-id': i}, segments_container);
        createElement('img', {'src': '/media/segment_marker_tx.png'}, s);
        s.addEventListener('click', function(event_) {
            console.log('click ' + this.dataset.id);
            var p = (event_.pageX-display_container.getBoundingClientRect().left)/display_container.getBoundingClientRect().width; 
            ap.onSegmentClick(this.dataset.id, ap, Math.min(Math.max(p,0),1));
        }, true);
        this._segments.push(s);
    }
    this._line = createElement('div', {'class': 'audio__indicator'}, display_container);
    var bar = createElement('div', {'class': 'audio__bar'}, this._element);
    this._label = createElement('div', {'class': 'audio__label'}, bar);
    var controls = createElement('ul', {'class': 'audio__control'}, bar);
    this._check = createElement('button', {'type': 'button', 'class': 'button icon-audio-on'}, createElement('li', null, controls));
    this._check.addEventListener('click', function () {
        ap.setMuted(!ap._muted);
        ps.setAudioState(ap._id, (ap._muted)? 0 : 1);
        ps.updateUrl();
    }, true);
    var extras = createElement('li', null, controls);
    createElement('button', {'type': 'button', 'class': 'button icon-more-ver more-toggler'}, extras);
    var more = createElement('ul', {'class': 'list--more'}, extras);
    this._form = createElement('button', {'type': 'button', 'class': 'button icon-graphic_eq', 'innerHTML': 'waveform'}, createElement('li', null, more));
    this._form.addEventListener('click', function () {
        var v = (ps.getAudioDisplay()=='s')? 'w' : 's';
        switchAudioDisplay(v);
    }, true);
    this._enlarge = createElement('button', {'type': 'button', 'class': 'button icon-zoom_in', 'innerHTML': 'enlarge'}, createElement('li', null, more));
    this._enlarge.addEventListener('click', function () {
        window.open(ap._enlarge_url);
    }, true);
    this._photo = createElement('button', {'type': 'button', 'class': 'button icon-photo_camera', 'innerHTML': 'photo'}, createElement('li', null, more));
    this._photo.addEventListener('click', function () {
        window.open(ap._photo_url);
    }, true);
    this._player = createElement('audio', {'id': 'audio_' + id}, this._element);
    this._player.addEventListener('ended', function() { ap.onAudioEnded(); }, true);
    this._player.addEventListener('timeupdate', function() { ap.onTimeUpdate(this.currentTime); }, true);
    this._muted = false;
    this._segment = 0;
    this._duration = 10 * 60;
    this._active = false;
    this._width = 1400;
    this.getElement = function() {
        return this._element;
    }
    this.hide = function () {
        this._element.style = 'display: none;';
        this._active = false;
    }
    this.show = function () {
        this._element.style = 'display: block;';        
        this._active = true;
    }
    this.play = function(seconds_) {
        if (seconds_) {
            this.setCurrentTime(seconds_);
        }
        this._player.play();
    }
    this.pause = function () {
        this._player.pause();
    }
    this.setMuted = function(muted_) {
        this._muted = muted_;
        if (this._muted) {
            this._check.classList.remove('icon-audio-on');
            this._check.classList.add('icon-audio-off');
        } else {
            this._check.classList.remove('icon-audio-off');
            this._check.classList.add('icon-audio-on');
        }
        this._player.muted = this._muted;
    }
    this.getMuted = function() {
        return this._muted;
    }
    this.setSegment = function(segment_) {
        // 0-based integer
        this._segment = segment_;
        for (var i=0; i<this._segments.length; i++) {
            var s = this._segments[i];
            var v = (i == this._segment)? 'hidden' : 'visible';
            s.style = 'visibility: ' + v + ';';
        }
        this.setLine(0);
    }
    this.setSource = function (media_, vis_, hours_, minutes_, data_) {
        this._label.innerHTML = (data_['description'])? data_['description'] + ' / ' + mics[data_['mic']] : mics[data_['mic']];
        this._player.src = '/media/tx/' + media_ + '/' + hours_ + '-' + minutes_ + '-' + data_['identifier'] + '.mp3';
        this._photo_url = '/media/' + data_['photo'];
        this._photo.style.display = (data_['photo']!='')? 'inline' : 'none';
        this.selectVisualization(vis_);
        this.show();
    }
    this.selectVisualization = function (vis_) {
        var path = this._player.src.split('/');
        var file = path.pop();
        path.push(vis[vis_] + 's');
        path.push(file.split('-')[0]+'-00-'+file.split('-').pop().split('.')[0]+'.png');
        this._display.src = path.join('/');
        this._enlarge_url = path.join('/').replace('.png', '-large.png');
        this._form.innerHTML = (vis_=='s')? vis['w'] : vis['s'];
    }
    this.setCurrentTime = function (seconds_) {
        this._player.currentTime = seconds_;
    }
    this.setLine = function(seconds_) {
        var lineX = (seconds_ + this._segment * this._duration) / (this._segments.length * this._duration) * this._element.offsetWidth;
        this._line.style = 'left: ' + lineX + 'px;';
    }
    this.onTimeUpdate = function(time_) {
        this.setLine(time_);
    }
    this.onAudioEnded = function() {
        goNextSegment(true);
    }
    this.onSegmentClick = function(id_, audio_, pos_) {
        var min = ('0' + (10 * id_)).slice(-2);
        var playing = !video.paused;
        ps.setAutoPlay(playing);
        ps.setMinutes(min);
        ps.updateUrl();
        updateInterface();
        updateMedia();
        // TODO: do I have to wait here for media to be loaded?
        onAudioClick(audio_, pos_);
        if (playing) {
            playAll();
        }
    }
    this.getId = function() {
        return this._id;
    }
    this.hide();
}

function updateInterface() {
    timeSelectors.map(function (item) {
        item.setSelected(item.getHour() == ps.getHours(true));
    });
    thumbs.map(function (item) {
        item.setTime(ps.getHours(true), ps.getMinutes(true));
        item.setSelected(ps.getLocation()==item.getIdentifier());
    });
    var l = locations[ps.getLocation()];
    info.setLabel(l.label);
    info.setMapLink(l.gps_lat, l.gps_long);
    var a = ps.getAudioDisplay();
    var s = ps.getSegment();
    audios.map(function (item) {
        item.setMuted(!(Boolean(ps.getAudioState(item.getId()))));
        item.setSegment(s);
    });
}

function updateAudioDisplays() {
    var display = ps.getAudioDisplay();
    audios.map(function (item) {
        item.selectVisualization(display);
    });
}

function updateMedia() {
    console.log('updateMedia ', ps.getLocation(), ps.getHours(true), ps.getMinutes(true, true));
    var media = locations[ps.getLocation()].media;
    var hours = ps.getHours(true);
    var minutes = ps.getMinutes(true, true);
    // location thumbs
    var ti = 1;
    var loc, t_url;
    var v_url = '/media/tx/' + media + '/' + hours + '-' + minutes + '.mp4';
    if (!video.paused) {
        video.pause();
    }
    video.src = v_url;
    var display = ps.getAudioDisplay();
    var media = locations[ps.getLocation()].media;
    var hours = ps.getHours(true);
    var minutes = ps.getMinutes(true, true);
    for (var i=0; i<3; i++) {
        if (locations[ps.getLocation()].is_real && i < locations[ps.getLocation()].audiorecordings.length) {
            audios[i].setSource(media, display, hours, minutes, locations[ps.getLocation()].audiorecordings[i]);
        } else {
            audios[i].hide();
        }
    }
}

function showLocation(location_) {
    ps.setLocation(location_);
    updateInterface();
    updateMedia();
}

function switchAudioDisplay(type_) {
    ps.setAudioDisplay(type_);
    updateInterface();
    updateAudioDisplays();
    ps.updateUrl();
}

function togglePlay() {
    if (video.paused) {
        playAll();
    } else {
        pauseAll();
    }
}

function playAll() {
    video.play();
    // audios play when play event of the video is fired
}

function pauseAll() {
    video.pause();
    // audios pause when play event of the video is fired
}

function gotoSec(time_) {
    // console.log('gotoSec', time_);
    video.currentTime = time_;
    audios.map(function (item) {
        item.setCurrentTime(time_);
    });
}

function gotoHour(hour_) {
    ps.setHours(hour_);
    updateInterface();
    updateMedia();
}

function goPrevHour() {
    var newhour = (ps.getHours(false) + 23) % 24;
    gotoHour(newhour);
}

function goNextHour() {
    var newhour = (ps.getHours(false) + 1) % 24;
    gotoHour(newhour);
}

function goPrevSec() {
    pauseAll();
    var targetSec = video.currentTime-1;
    if (targetSec>=0) {
        video.currentTime = targetSec;
    } else {
        // TODO: jump to the last frame of the previous segment
        console.log('jump to end of previous segment not yet implemented');
    }
}

function goNextSec() {
    pauseAll();
    var targetSec = video.currentTime+1;
    if (targetSec<=video.duration) {
        video.currentTime = targetSec;
        pauseAll();
    } else {
        goNextSegment(false);
    }
}

function onPlayPauseClick() {
    if (video.paused) {
        playAll();
    } else {
        pauseAll();
    }
}

function onTimeUpdate(time_) {
    if (video.paused) {
        audios.map(function (item) {
            item.setCurrentTime(time_);
            item.setLine(time_);
        });
    }
}

function onVideoPlay() {
    controls.setPlayPause(true);
    audios.map(function (item) {
        item.play();
    });
}

function onVideoPause() {
    controls.setPlayPause(false);
    audios.map(function (item) {
        item.pause();
    });
}

function onVideoEnded() {
    goNextSegment(true);
}

function goNextSegment(play_) {
    ps.setAutoPlay(play_);
    var s = ps.getSegment();
    s = (s+1)%6;
    if (s==0) {
        // jump to next hour
        var h = (ps.getHours()+1)%24;
        gotoHour(h);
    } else {
        ps.setMinutes(s*10);
        ps.updateUrl();
        updateInterface();
        updateMedia();
    }
}

function onAudioClick(audio_, pos_) {
    if (audio_.getMuted()) {
        audios.map( function(item) {
            item.setMuted(item!=audio_);
        });
        ps.updateUrl();
    }
    var segment = ps.getSegment();
    var segments = 6;
    var totalSec = 60 * 60;
    var targetSec = pos_ * totalSec;
    if (segment>0) {
        targetSec %= segment/segments*totalSec;
    }
    ps.setAutoPlay(!video.paused);
    gotoSec(targetSec);
}

// player state
var ps = new PlayerState();


// =============
// this has to be merged with main.js
// =============
// window.onpopstate = function(event) {
//     if (window.location.hash !== ps.toString() && document.getElementById('tx_player')) {
//         ps.parseHash(window.location.hash);
//         updateInterface();
//         updateMedia();
//     }
// };

const DEBUG = false;


// https://computersignale.zhdk.ch/de/about/exhibitions:
// Fotodokumentation Marc Latzel [Download PDF]  –> Link?? Leaflet Ausstellung [Download PDF|; Audiodokumentation –> Link?? Audopieces von Valentina (in Absprache mit ihr): Audiodok Walcheturm Omnimics; Kompilation TACC, Supercomputer; Kompilation -80°-Freezeer; ev. weitere.

var video; // video container
var playPause = null; // play/pause control element

function initTXDocumentation() {
    // prepare page
    var page = document.getElementById('page');
    page.innerHTML = '';
    var pageBody = document.createElement('div');
    pageBody.id = 'page__body';

    // title
    var title = document.createElement('h1');
    if (sessionStorage.getItem("lang") == "de") {
        title.innerHTML = "Afrikanischer Buntbarsch #3, Soundscape Texas. Kunstraum Walcheturm, Zürich 2020 (mit Valentina Vuksic, Birk Weiberg, Christoph Stähli) (EA)";
    } else {
        title.innerHTML = "African Cichlid #3, Soundscape Texas. Kunstraum Walcheturm, Zurich 2020 (with Valentina Vuksic, Birk Weiberg, Christoph Stähli) (SE)";
    }
    pageBody.appendChild(title);

    // var map_img = document.createElement('img');
    // map_img.setAttribute('src', '/media/HR_Afrikanischer_Buntbarch_3_Dia_1920x1080px_72dpi.png');
    // pageBody.append(map_img);

    // video
    createVideo(pageBody);

    // leaflet
    var leaflet = document.createElement('h1');
    var leaflet_pdf = document.createElement('a');
    leaflet_pdf.setAttribute('class', 'link--media');
    if (sessionStorage.getItem("lang") == "de") {
        leaflet_pdf.setAttribute('href', '/media/HR_Leaflet_A5_DIGITAL_DEF_de.pdf');
        leaflet_pdf.innerHTML = "Saaltext";
    } else {
        leaflet_pdf.setAttribute('href', '/media/HR_Leaflet_A5_DIGITAL_DEF_en.pdf');
        leaflet_pdf.innerHTML = "Exhibition text";
    }
    leaflet.appendChild(leaflet_pdf);
    pageBody.appendChild(leaflet);


    // PDF Dokumentation
    var doc = document.createElement('h1');
    var doc_pdf = document.createElement('a');
    doc_pdf.setAttribute('class', 'link--media');
    if (sessionStorage.getItem("lang") == "de") {
        doc_pdf.setAttribute('href', '/media/Buntbarsch3_Dok-Walcheturm2020_de_lr.pdf');
        doc_pdf.innerHTML = "PDF";
    } else {
        doc_pdf.setAttribute('href', '/media/Cichlid3_Doc-Walcheturm2020_en_lr.pdf');
        doc_pdf.innerHTML = "PDF";
    }
    doc.appendChild(doc_pdf);
    pageBody.appendChild(doc);


    // flyer
    var flyer = document.createElement('h1');
    var flyer_pdf = document.createElement('a');
    flyer_pdf.setAttribute('class', 'link--media');

    flyer_pdf.setAttribute('href', '/media/HR_Flyer_A5_LF_fin.pdf');
    flyer_pdf.innerHTML = "Flyer";

    flyer.appendChild(flyer_pdf);
    pageBody.appendChild(flyer);


    // map
    // var map = document.createElement('h1');
    //
    // var map_link = document.createElement('a');
    // map_link.setAttribute('href', '/media/HR_Afrikanischer_Buntbarch_3_Dia_1920x1080px_72dpi.png')
    // map_link.setAttribute('class', 'link--media')
    // if (sessionStorage.getItem("lang") == "de") {
    //     map_link.innerHTML = "Geographische Karte «Afrikanischer Buntbarsch #3»"
    // } else {
    //     map_link.innerHTML = "Geographic map «African Cichlid #3»"
    // }
    //
    // // var map_img = document.createElement('img');
    // // map_img.setAttribute('src', '/media/HR_Afrikanischer_Buntbarch_3_Dia_1920x1080px_72dpi.png');
    //
    // map.appendChild(map_link);
    // pageBody.appendChild(map)

    // pageBody.appendChild(map_img);

    page.appendChild(pageBody);


}

function createVideo(pageBody) {

    // var titleVideoDocumentation = document.createElement('h1');
    // if (sessionStorage.getItem("lang") == "de") {
    //     titleVideoDocumentation.innerHTML = "Videodokumentation";
    // } else {
    //     titleVideoDocumentation.innerHTML = "Video documentation";
    // }
    // pageBody.appendChild(titleVideoDocumentation);

    var txplayer = document.createElement('div');
    txplayer.setAttribute('id', 'tx-player');

    pageBody.appendChild(txplayer);

    if (sessionStorage.getItem("lang") == "de") {
        var sources = {
            'video/mp4': '/media/tx/documentation/video/HR_WALCHETURM_03_01_UHD_final_DE_115.07.20abr10000_h264.mp4',
            'video/webm': '/media/tx/documentation/video/HR_WALCHETURM_03_01_UHD_final_DE_115.07.20abr10000.webm'
        };

        var imageSrc = '/media/tx/documentation/video/HR_WALCHETURM_03_01_UHD_final_DE_115.07.20abr10000.png';

    } else {
        var sources = {
            'video/mp4': '/media/tx/documentation/video/HR_WALCHETURM_03_01_HD_final_EN_115.07.20abr10000_h264.mp4',
            'video/webm': '/media/tx/documentation/video/HR_WALCHETURM_03_01_HD_final_EN_115.07.20abr10000.webm'
        };

        var imageSrc = '/media/tx/documentation/video/HR_WALCHETURM_03_01_HD_final_EN_115.07.20abr10000.png';
    }


    loadVideo(sources, imageSrc, txplayer);

}

function setPlayPause(play) {
    if (playPause != undefined) {
        if (play) {
            playPause.classList.remove('icon-play');
            playPause.classList.add('icon-pause');
        } else {
            playPause.classList.remove('icon-pause');
            playPause.classList.add('icon-play');
        }
    }

}

function loadVideo(sources, imageSrc, el) {
    if (DEBUG) console.debug('TXDocumentation - loadVideo');

    var video = document.createElement('video');

    Object.keys(sources).forEach(function (key) {
        var source = document.createElement('source');
        source.type = key;
        source.src = sources[key];

        video.appendChild(source);
    });

    // video.setAttribute('controls', '');
    video.setAttribute('width', '100%');
    video.setAttribute('poster', imageSrc);

    video.addEventListener('ended', function() {
        if (DEBUG) { console.debug('ended'); }
        setPlayPause(false);
    }, true);

    video.addEventListener('pause', function() {
        if (DEBUG) { console.debug('pause'); }
        setPlayPause(false);
    }, true);

    video.addEventListener('play', function() {
        if (DEBUG) { console.debug('play'); }
        setPlayPause(true);
    }, true);


    // define controls

    playPause = document.createElement('a');
    playPause.setAttribute('class', 'link icon-play click-item');
    playPause.setAttribute('title', 'Play/Pause');

    function togglePlay() {
        if (video.paused) {
            video.play();
            // setPlayPause(true);
        } else {
            video.pause();
            // setPlayPause(false);
        }
    }

    playPause.addEventListener('click', togglePlay);
    video.addEventListener('click', togglePlay);

    var fullscreen = document.createElement('a');
    fullscreen.setAttribute('class', 'link icon-fullscreen click-item');
    fullscreen.setAttribute('title', 'Fullscreen');

    function toggleFullscreen() {
        if (video.requestFullScreen) {
            video.requestFullScreen();
        } else if (video.webkitRequestFullScreen) {
            video.webkitRequestFullScreen();
        } else if (video.mozRequestFullScreen) {
            video.mozRequestFullScreen();
        }
    }

    fullscreen.addEventListener('click', toggleFullscreen);

    // add controls
    var controls = document.createElement('div');
    controls.setAttribute('id', 'timecontrol');
    controls.appendChild(playPause);
    controls.appendChild(fullscreen);

    // append video and controls
    el.appendChild(video);
    el.appendChild(controls);


}
DOKKU_HOST = computersignale.zhdk.ch
DOKKU_APP = csweb-prod
VERSION = latest
DOKKU_IMAGE_TAG = dokku/$(DOKKU_APP):$(VERSION)

# build image locally and deploy (server architecture must be the same as local)
deploy:
	docker build --no-cache -t $(DOKKU_IMAGE_TAG) .
	docker save $(DOKKU_IMAGE_TAG) | ssh $(DOKKU_HOST) "docker load"
	-ssh $(DOKKU_HOST) "dokku git:from-image $(DOKKU_APP) $(DOKKU_IMAGE_TAG)"
	ssh $(DOKKU_HOST) "dokku ps:rebuild $(DOKKU_APP)"
